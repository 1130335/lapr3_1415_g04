/* 
 * File:   Force.h
 * Author: Paulo Silva
 *
 * Created on 14 de Janeiro de 2015, 16:21
 */

#ifndef FORCE_H
#define	FORCE_H

#include <string>
#include <iostream>
using namespace std;

class Force{
private:
    double fX;
    double fY;
    double fZ;
    
public:
    Force();
    ~Force();
    Force(double, double, double);
    Force(Force&);
    
    double getFX() const;
    double getFY() const;
    double getFZ() const;
    
    void setFX(double);
    void setFY(double);
    void setFZ(double);
        
};


#endif	/* FORCE_H */

