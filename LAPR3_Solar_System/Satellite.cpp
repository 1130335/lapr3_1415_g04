#include "Satellite.h"

Satellite::Satellite():Objects(){
    
}

Satellite::Satellite(double v, double m, double g, double o, int id):Objects(v, m, g, o, id){
    
}

Satellite::Satellite(const Satellite& s):Objects(s){
    
}

Satellite::~Satellite() {
    
}

void Satellite::write(ostream& out) const{
    out << "Object Type: Satellite" << endl;
    Objects::write(out);
    out << endl;
}
