/* 
 * File:   VerletAlgorithm.h
 * Author: Eduardo Pinto <1130466@isep.ipp.pt>
 */

#ifndef VERLETALGORITHM_H
#define	VERLETALGORITHM_H
#include <math.h>
#include <time.h>
#include <vector>
#include "Force.h"
#include "SolarSystem.h"
#include "Simulation.h"
#include "Results.h"
#include "NaturalObjects.h"
using namespace std;


class VerletAlgorithm {
private:
    
    vector<double> mass;
    double G=pow(6.67,(-11));
    SolarSystem* ss;
    Simulation* s;
    Results* r;
    vector<Force*> force;
    vector<double> force_x;
    vector<double> force_y;
    vector<double> force_z;
    vector<double> force_x2;
    vector<double> force_y2;
    vector<double> force_z2;
    double t_s;
    double t_f; 
    double r_t;
    double timestep;
    double delta_T;

public:
    vector<double>ComputeForce(vector<double> mass, vector<double> x, vector<double> y, vector<double> z);
    void Integrate();
    VerletAlgorithm();
    ~VerletAlgorithm();
    double CalTimeStep();
    
    double getDelta_T();
    double getT_s();
    double getT_f();
    double getR_t();
    double getTimestep();
    vector<Force*> getForce();
    vector<double> getForce_x();
    vector<double> getForce_y();
    vector<double> getForce_z();
    vector<double> getForce_x2();
    vector<double> getForce_y2();
    vector<double> getForce_z2();
    vector<double> getMass(vector<NaturalObjects*>);
    
    void setDelta_T(double dt);
    void setT_s(double ts);
    void setT_f(double tf);
    void setR_T(double rt);
    void setTimestep(double ti);
    void setForce_x(vector<double> fx);
    void setForce_y(vector<double> fy);
    void setForce_z(vector<double> fz);
    void setForce_x2(vector<double> fx2);
    void setForce_y2(vector<double> fy2);
    void setForce_z2(vector<double> fz2);
    void setSimulation(Simulation* s);
    
};



#endif	/* VERLETALGORITHM_H */

