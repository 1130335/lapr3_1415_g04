#ifndef MINORPLANET_H
#define	MINORPLANET_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class MinorPlanet : public NaturalObjects {
public:
    vector<Moon*> moons;
    MinorPlanet();
    MinorPlanet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    MinorPlanet(MinorPlanet&);
    ~MinorPlanet();
    void write(ostream&);
    
    void addMoons(Moon*);
    void showMoons();
};

#endif	/* MINORPLANET_H */

