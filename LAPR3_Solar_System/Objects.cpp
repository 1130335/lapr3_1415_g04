#include "Objects.h"

Objects::Objects() {
    this->velocity = 0;
    this->mass = 0;
    this->graForce = 0;
    this->orbit = 0;
    this->id = 1;
}

Objects::Objects(double v, double m, double g, double o, int id) {
    this->velocity = v;
    this->mass = m;
    this->graForce = g;
    this->orbit = o;
    this->id = id;

}

Objects::Objects(const Objects& ob) {
    this->velocity = ob.getVelocity();
    this->mass = ob.getMass();
    this->graForce = ob.getGraForce();
    this->orbit = ob.getOrbit();
    this->id = ob.getID();
}

Objects::~Objects() {

}

void Objects::setVelocity(double v) {
    this->velocity = v;
}

void Objects::setMass(double m) {
    this->mass = m;
}

void Objects::setGraForce(double f) {
    this->graForce = f;
}

void Objects::setOrbit(double o) {
    this->orbit = o;
}

void Objects::setID(int id) {
    this->id = id;
}

double Objects::getVelocity() const {
    return this->velocity;
}

double Objects::getID() const {
    return this->id;
}

double Objects::getMass() const {
    return this->mass;
}

double Objects::getGraForce() const {
    return this->graForce;
}

double Objects::getOrbit() const {
    return this->orbit;
}

void Objects::write(ostream& out) const {
    out << "Velocity: " << velocity << endl;
    out << "Mass: " << mass << endl;
    out << "Gravitational force: " << graForce << endl;
    out << "Orbit: " << orbit << endl;
}

ostream & operator<<(ostream &out, Objects& o) {
    o.write(out);
    return out;
}
