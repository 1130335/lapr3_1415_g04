#include "MenuUI.h"
#include "CreateSSController_FT.h"

using namespace std;

int main(int argc, char** argv) {

    /*
     * Ligação a base de dados
     */

    /*
	string utilizador = "lapr3_004"; // substituir pelo correcto
	string palavra = "qwerty"; // substituir pela correcta
	string bd = "gandalf.dei.isep.ipp.pt:1521/pdborcl"; //ou em vez de ip, gandalf
	try {
		
		BDados *connection = new BDados(utilizador, palavra, bd);
		cout << "Ligado com sucesso a base de dados!" << endl;
		/*
		list <SolarSystem*> l = connection->lerSistemaSolar();
		cout << setw(5) << "COD." << " | "
			<< setw(12) << "Nome" << " | "
			<< setw(6) << "NIF" << " | "
			<< setw(14) << "Morada" << " | " << endl;
		
		for (list<SolarSystem>::iterator it = l.begin(); it != l.end(); it++)
			cout << *it;
		cout << endl;
		
		delete (connection);
		cout << "Exemplo de ligacao: terminado" << endl;
	}
	catch (SQLException erro) {
		cerr << "Erro: " << erro.getMessage() << endl;
	}
	cin.get();
	*/

    MenuUI mui;
    mui.run();

    return 0;
}
