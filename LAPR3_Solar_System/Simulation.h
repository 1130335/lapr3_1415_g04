#ifndef SIMULATION_H
#define	SIMULATION_H

#include "Star.h"
#include "SolarSystem.h"
#include "NaturalObjects.h"
#include "Objects.h"
#include <string>

using namespace std;

class Simulation {
private:
    Star s_star;
    vector <NaturalObjects*> bodys;
    vector <Objects*> objects;
    SolarSystem* solar;
    string name;
    double start_time;
    double end_time;
    double time_step;
    int reference_time;
    double delta_time;

public:
    Simulation();
    Simulation(SolarSystem*, string, double, double, double, int);
    Simulation(Simulation&);
    ~Simulation();

    void setName(string name);
    void setStart_Time(double start_time);
    void setEnd_Time(double end_time);
    void setTime_Step(double time_step);
    void setReference_Time(int rt);
    void setSolarSystem(SolarSystem*);
    void setDelta_Time(double);

    SolarSystem* getSolar() const;
    string getName() const;

    double getMass(NaturalObjects*);
    double getStart_Time();
    double getEnd_Time();
    double getTime_Step();
    double getDelta_Time();
    int getReference_time();
    vector<NaturalObjects*> getVNObj();
    vector<Objects*> getVObj();
    void setVNObj(vector<NaturalObjects*> vno);
    void setVObj(vector<Objects*> vo);

    void addBodys(NaturalObjects*);
   
    void addObjects(Objects*);

    void write(ostream& out) const;
    
    vector<double> getPosition(NaturalObjects* no);
    vector<double> getVelocity(NaturalObjects* no);
    
    friend ostream & operator<<(ostream &out, Simulation& s);

};

#endif	/* SIMULATION_H */

