/* 
 * File:   Results.h
 * Author: Paulo Silva
 *
 * Created on 9 de Janeiro de 2015, 14:00
 */

#ifndef RESULTS_H
#define	RESULTS_H

#include <vector>
#include <string>
using namespace std;

class Results{
private:
    vector<double> xVel;
    vector<double> yVel;
    vector<double> zVel;
    vector<double> xPos;
    vector<double> yPos;
    vector<double> zPos;
    vector<double> timestep;

public:
    Results();
    ~Results();
    vector<double> getXVel();
    vector<double> getYVel();
    vector<double> getZVel();
    vector<double> getXPos();
    vector<double> getYPos();
    vector<double> getZPos();
    vector<double> getTimestep();
    
    void setXVel(vector<double> xV);
    void setYVel(vector<double> yV);
    void setZVel(vector<double> zV);
    void setXPos(vector<double> xP);
    void setYPos(vector<double> yP);
    void setZPos(vector<double> zP);
    void setTimestep(vector<double> ts);
  
    void inserirXvel(double xV);
    void inserirYvel(double yV);
    void inserirZvel(double zV);
    void inserirXpos(double xP);
    void inserirYpos(double yP);
    void inserirZpos(double zP);
    void inserirTimeStep(double ts);
};

#endif	/* RESULTS_H */

