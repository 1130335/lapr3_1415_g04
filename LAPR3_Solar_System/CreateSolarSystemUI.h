#ifndef CREATESOLARSYSTEMUI_H
#define	CREATESOLARSYSTEMUI_H

#include "CreateSolarSystemController.h"
#include "SolarSystemList.h"
#include "SolarSystem.h"

#include <iostream>
#include <string>

using namespace std;

class CreateSolarSystemUI {
private:
    CreateSolarSystemController cssc;
    
    // Body's Variables
    string n;
    string o;
    double px;
    double py;
    double pz;
    double vx;
    double vy;
    double vz;
    double pr;
    double os;
    double iao;
    double sa;
    double e;
    double oi;
    double ed;
    double m;
    double d;
    double ev;
    double p;
    double a;

    // Object's Variables
    double mass;
    double velocity;
    double orbit;
    double force;
    int id;

public:
    CreateSolarSystemUI();
    ~CreateSolarSystemUI();

    void createStar();
    void getBodyData();
    void createNaturalObjects();
    void getObjectData();
    void createManMadeObjects();

    void run(SolarSystemList&);
};

#endif	/* CREATESOLARSYSTEMUI_H */

