#include "StartSimulationController.h"

StartSimulationController::StartSimulationController() {
}

StartSimulationController::~StartSimulationController() {

}

void StartSimulationController::setStart_time(double st) {
    s.setStart_Time(st);
}

void StartSimulationController::setEnd_time(double et) {
    s.setEnd_Time(et);
}

void StartSimulationController::setTime_Step(double ts) {
    s.setTime_Step(ts);
}

void StartSimulationController::setReference_time(int rt){
    s.setReference_Time(rt);
}

void StartSimulationController::setName(string n) {
    s.setName(n);
}

void StartSimulationController::setPosition(double px, double py, double pz) {
    s.getVNObj().back()->setPx(px);
    s.getVNObj().back()->setPy(py);
    s.getVNObj().back()->setPz(pz);
    //s.getVNObj().back()->setPosition(px, py, pz);
}

void StartSimulationController::setVelocity(double vx, double vy, double vz) {
    s.getVNObj().back()->setVx(vx);
    s.getVNObj().back()->setVy(vy);
    s.getVNObj().back()->setVz(vz);
    //s.getVNObj().back()->setVelocity(vx, vy, vz);
}

void StartSimulationController::showListNO() {
    SolarSystem* ss = s.getSolar();
    list<NaturalObjects*> aux = ss->getNObj();
    int size = aux.size();
    for (int i = 0; i < size; i++) {
        cout << i + 1 << " - " << aux.front()->getName() << endl;
        aux.pop_front();
    }
}

void StartSimulationController::addNO(int i) {
    int cont = 1;
    SolarSystem* ss = s.getSolar();
    NaturalObjects* no = new NaturalObjects();
    list<NaturalObjects*> aux = ss->getNObj();
    while (cont != i) {
        aux.pop_front();
        cont++;
    }

    no = aux.front();
    setReference(no->getReference());
    s.addBodys(no);
}

void StartSimulationController::setReference(string ref){
    reference = ref;
}

vector<string> StartSimulationController::splitDate(string reference) {

    vector<string> data;

    int slash;
    slash = reference.find("/", 0);
    data[0] = reference.substr(0, slash);

    reference.erase(0, slash);
    slash = reference.find("/", 0);
    data[1] = reference.substr(0, slash);

    reference.erase(0, slash);
    data[2] = reference;

    return data;

}

int StartSimulationController::referenceTime(int rt, vector<string> data) {

    int dia, mes, ano;

    dia = atoi(data[0].c_str());
    mes = atoi(data[1].c_str());
    ano = atoi(data[2].c_str());

    dia = (dia * 86400);
    mes = (mes * 2628000);
    ano = (ano * 31535999999964777);

    rt = dia + mes + ano;
    
    return rt;
}

void StartSimulationController::showListO() {
    SolarSystem* ss = s.getSolar();
    list<Objects*> aux = ss->getObj();
    int size = aux.size();
    for (int i = 0; i < size; i++) {
        if (typeid (*aux.front()) == typeid (Satellite)) {
            cout << i + 1 << " - " << "Satellite" << endl;
        } else {
            cout << i + 1 << " - " << "Space-Ship" << endl;
        }
        aux.pop_front();
    }
}

void StartSimulationController::addO(int i) {
    int cont = 1;
    SolarSystem* ss = s.getSolar();
    Objects* o = new Objects();
    list<Objects*> aux = ss->getObj();
    while (cont != i) {
        aux.pop_front();
        cont++;
    }

    o = aux.front();
    s.addObjects(o);
}

void StartSimulationController::setSolarSystem(SolarSystem* ss) {
    s.setSolarSystem(ss);
}

Simulation& StartSimulationController::getSimulation() {
    return s;
}