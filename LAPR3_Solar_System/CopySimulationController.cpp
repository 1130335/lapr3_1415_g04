#include "CopySimulationController.h"

CopySimulationController::CopySimulationController() {
}

CopySimulationController::~CopySimulationController() {
}

void CopySimulationController::showList(SimulationList& sl) {
    cout << sl;
}

Simulation* CopySimulationController::getSimulation(int s, SimulationList& sl) {
    int cont = 1;
    Simulation* ss;
    list<Simulation*> aux = sl.getSimulationList();
    
    while(cont != s) {
        aux.pop_front();
        cont++;
    }
    
    ss = aux.front();
    
    return ss;
}

void CopySimulationController::copySimulation(Simulation* ss, SimulationList& sl) {
    Simulation* sc = new Simulation();
    sc->setVNObj(ss->getVNObj());
    sc->setVObj(ss->getVObj());
    sc->setSolarSystem(ss->getSolar());
    sc->setStart_Time(ss->getStart_Time());
    sc->setEnd_Time(ss->getEnd_Time());
    sc->setTime_Step(ss->getTime_Step());
    sc->setName(ss->getName());
    sc->setDelta_Time(ss->getDelta_Time());
    sl.add(sc);
}
