#ifndef DELETESIMULATIONUI_H
#define	DELETESIMULATIONUI_H

#include <iostream>
#include "SimulationList.h"
#include "DeleteSimulationController.h"

using namespace std;

class DeleteSimulationUI {

public:
   
    DeleteSimulationUI();
    DeleteSimulationUI(const DeleteSimulationUI& orig);
    ~DeleteSimulationUI();

    void run(SimulationList&);
    
private:

    DeleteSimulationController dsc;
    
};

#endif	/* DELETESIMULATIONUI_H */

