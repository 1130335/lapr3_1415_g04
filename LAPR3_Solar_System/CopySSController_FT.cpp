#include "CopySSController_FT.h"

CopySSController_FT::CopySSController_FT() {
    ss1 = new SolarSystem();
}

CopySSController_FT::~CopySSController_FT() {
}

void CopySSController_FT::run() {
    cout << "TESTING COPYSOLARSYSTEMCONTROLLER" << endl;
    sslTest.add(ss1);
    cssc.showList(sslTest);
    SolarSystem* ss2 = cssc.getSolarSystem(1, sslTest);
    cssc.copySolarSystem(ss2, sslTest);
    cssc.showList(sslTest);
    cout << "Test Complete!!!" << endl;
}
