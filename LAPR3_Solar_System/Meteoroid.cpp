#include "Meteoroid.h"

Meteoroid::Meteoroid() : NaturalObjects() {
    this->setEquatorialDiameter(0.0000010);
}

Meteoroid::Meteoroid(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) : NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz, o) {

}

Meteoroid::Meteoroid(Meteoroid& m) : NaturalObjects(m) {

}

Meteoroid::~Meteoroid() {

}

void Meteoroid::write(ostream& out) {
    out << "Body Type: Meteoroid" << endl;
    NaturalObjects::write(out);
    out << endl;
}

void Meteoroid::addMoons(Moon* m) {
    this->moons.push_back(m);
}

void Meteoroid::showMoons() {
    for (int i = 0; i < this->moons.size(); i++) {
        cout << *this->moons[i];
    }
}

