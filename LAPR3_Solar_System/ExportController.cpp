#include "ExportController.h"

ExportController::ExportController(){
    this->s = new Simulation();
    
}

ExportController::~ExportController(){
    
}

void ExportController::verificaList(){
}
 
 void ExportController::exportCSV(Simulation* si){
     this->s = new Simulation();
     this->s = si;
    
        stringstream st;
	st << si->getName() << ".csv";
	string name = st.str();
	char * n = new char[name.length() + 1];
	strcpy(n, name.c_str());
	ofstream csv(n);
        vector <NaturalObjects*> aux;
        SolarSystem* aux2;
        
        csv << "Simulation;Solar System;Start Time;End Time;Time Step;Name of Objects";
        aux = si->getVNObj();
        while (!aux.empty()){
            csv << ";" << aux.back()->getName();
            aux.pop_back();
        }
        csv << endl;
         
        
		SolarSystem* solar = new SolarSystem();
        aux2 = si->getSolar();
        csv << ";" << solar;
        csv << endl;
   
       
        double start_time;
        start_time = si->getStart_Time();
        while (!aux.empty()) {
            csv << ";" << start_time;
            aux.pop_back();
        }
        csv << endl;
        
        
        double end_time;
        end_time = si->getEnd_Time();
        while (!aux.empty()) {
            csv << ";" << end_time;
            aux.pop_back();
        }
        csv << endl;
        
        for (int i = 0; i < aux.size(); i++) {
        csv << ";" << aux[i]->getName();
        }
        csv << endl;
        
    csv << "Time Step";
	double time_step;
    time_step = si->getStart_Time();
    while (!aux.empty()) {
        csv << ";" << time_step;
        aux.pop_back();
        
    int i,j, timestep;
    int size = r.getXPos().size();
    //timestep = s->getTime_Step();
    vector <NaturalObjects*> bodys;
    vector <Objects*> objects;
    
    bodys = s->getVNObj();
    objects = s->getVObj();
    
    vector<double> xVel;
    vector<double> yVel;
    vector<double> zVel;
    vector<double> xPos;
    vector<double> yPos;
    vector<double> zPos;
    
    xVel = r.getXVel();
    yVel = r.getYVel();
    zVel = r.getZVel();
    xPos = r.getXPos();
    yPos = r.getYPos();
    zPos = r.getZPos();
    
    string novosDados;
        for (i = 0; i < s->getEnd_Time(); i = (i + time_step)) {
            for (j = 0; j < size; j++) {

                novosDados = "<tr>"
                        , ";", i, "</td>\n"
                        , ";", bodys[j]
                        , ";", xPos[i]
                        , ";", yPos[i]
                        , ";", zPos[i]
                        , ";", xVel[i]
                        , ";", yVel[i]
                        , ";", zVel[i];

                csv << novosDados;
                csv << endl;
            }
        }
    }
}