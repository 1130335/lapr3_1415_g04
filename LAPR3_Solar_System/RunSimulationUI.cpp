#include "RunSimulationUI.h"
#include "RunSimulationController.h"

RunSimulationUI::RunSimulationUI() {
}

RunSimulationUI::~RunSimulationUI() {
}

void RunSimulationUI::run(SimulationList& sl) {

    Simulation* s = new Simulation();
    int newVB = 1;
    cout << "\nRUN SIMULATION" << endl;
    cout << "Choose one of the following simulations to run: " << endl;
    runC.showList(sl);
    int sim;
    cout << "Answer: ";
    cin >> sim;
    s = runC.chooseSimulation(sim, sl);
    while (newVB == 1) {
        runC.runVerlet(s);
        cout << "Verlet Algorithm Calculated" << endl;
        cout << "\nDo you wish to run the simulation for another body? \n0 - No \n1 - Yes" << endl;
        cout << "Answer: ";
        cin >> newVB;
    }

    cout << "\nSIMULATION RAN SUCCESSFULLY\n\n";
}



