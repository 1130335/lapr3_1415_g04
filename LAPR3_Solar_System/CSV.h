#ifndef CSV_H
#define	CSV_H

#include "SolarSystemList.h"
#include "SolarSystem.h"
#include "Planet.h"
#include "Moon.h"
#include <string>
#include <list>
#include <sstream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

using namespace std;

class CSV {
private:
	SolarSystemList ssl;
public:
	CSV();
	~CSV();
	void createCSV_SS(SolarSystem&);
	SolarSystem* importSolarSystem(string);
        void type3File(list<list<string>* >, SolarSystem*);
};

#endif	/* CSV_H */

