#include "SimulationList.h"

SimulationList::SimulationList() {
}

SimulationList::SimulationList(list<Simulation*> l) {
}

SimulationList::~SimulationList() {
}

void SimulationList::add(Simulation* s) {
    this->sl.push_back(s);
}

void SimulationList::remove() {
    this->sl.pop_back();
}

void SimulationList::showSimulations(ostream& out) {
    list<Simulation*> aux = this->sl;
    int i = 1;
    while(!aux.empty()) {
        cout << "\t" << i << " - " << aux.front()->getName() << endl;
        i++;
        aux.pop_front();
    }
}

ostream & operator<<(ostream &out, SimulationList& l) {
    l.showSimulations(out);
    return out;
}

list<Simulation*> SimulationList::getSimulationList() const{
    return sl;
}

void SimulationList::setSimulationList(list<Simulation*> listSS){
    sl = listSS;
}