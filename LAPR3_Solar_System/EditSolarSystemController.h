/* 
 * File:   EditSolarSystemController.h
 * Author: Rui Correia <1130458@isep.ipp.pt>
 *
 * Created on 9 de Janeiro de 2015, 15:07
 */

#ifndef EDITSOLARSYSTEMCONTROLLER_H
#define	EDITSOLARSYSTEMCONTROLLER_H

#include "SolarSystem.h"
#include "Star.h"
#include "Objects.h"
#include "NaturalObjects.h"

using namespace std;

class EditSolarSystemController {

private:
    list<SolarSystem*> lss;
    list<Objects*> obj;
    list<NaturalObjects*> nObj;
    Star system_star;
    
    SolarSystem* chosen;
    
public:
    EditSolarSystemController();
    ~EditSolarSystemController();
    
    int alterListaSistemasSolares();
    SolarSystem* getInfoSS(int);
    
    void setChosen(SolarSystem* ss);
    
    int alterNaturalBody();
    void setNB(list<NaturalObjects*> listaNB);
    list<NaturalObjects*> getNB() const;
    void addNewNaturalBody(NaturalObjects* newNO);
    void deleteNB(int index);
    
    int alterManMadeObject();
    void addNewManMadeObject(Objects* Newobj);
    void deleteMMO(int index);
    void setMMO(list<Objects*> listaMMO);
    list<Objects*> getMMO() const;
    
    
    list<SolarSystem*> getLSS() const;
    void setLSS(list<SolarSystem*> listaSS);
    
};

#endif	/* EDITSOLARSYSTEMCONTROLLER_H */

