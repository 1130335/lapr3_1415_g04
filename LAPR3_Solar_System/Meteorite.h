#ifndef METEORITE_H
#define	METEORITE_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Meteorite : public NaturalObjects {
private:

public:
    vector<Moon*> moons;
    Meteorite();
    Meteorite(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    Meteorite(Meteorite& m);
    void write(ostream& out);

    void addMoons(Moon*);
    void showMoons();
};

#endif	/* METEORITE_H */

