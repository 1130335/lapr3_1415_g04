#ifndef CREATESOLARSYSTEMCONTROLLER_H
#define	CREATESOLARSYSTEMCONTROLLER_H

#include "CSV.h"
#include "SolarSystem.h"
#include "SolarSystemList.h"
#include "Planet.h"
#include "Star.h"
#include "MinorPlanet.h"
#include "Asteroid.h"
#include "Fireball.h"
#include "Meteoroid.h"
#include "Body.h"
#include "Bolide.h"
#include "Meteor.h"
#include "Meteorite.h"
#include "Comet.h"
#include "Moon.h"
#include "Satellite.h"
#include "SpaceShip.h"

#include <sstream>
#include <string>

using namespace std;

class CreateSolarSystemController {
private:
    SolarSystem ss;
    CSV ss_file;
    
public:
    CreateSolarSystemController();
    ~CreateSolarSystemController();
    bool nObjNotEmpty();

    void createStar(string, double, double);
    void createPlanet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createMinorPlanet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createAsteroid(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string, double);
    void createFireball(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createMeteoroid(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createBody(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createBolide(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createMeteor(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createMeteorite(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createComet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    void createMoon(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);

    void createSatellite(double, double, double, double, int);
    void createSpaceShip(double, double, double, double, int);
    
    void addSSToList(SolarSystemList&);
    
    void createCSVFile();
};

#endif	/* CREATESOLARSYSTEMCONTROLLER_H */

