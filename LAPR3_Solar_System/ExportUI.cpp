#include "ExportUI.h"
#include "ExportHTML.h"

ExportUI::ExportUI() {

}

ExportUI::~ExportUI(){
    
}

void ExportUI::run(SimulationList& sl) {
    int opcao = -1;
    int opcao2;
    int a;
    cout << "EXPORT SIMULATION FOR HTML OR CSV" << endl;
    if (!sl.getSimulationList().empty()) {
        cout << "List of Simulations" << endl;
        cout << sl;
        cout << "Choose one of the previous Simulations" << endl;
        cin >> a;

        int cont = 1;
        Simulation* ss;
        list<Simulation*> aux = sl.getSimulationList();

        while (cont != a) {
            aux.pop_front();
            cont++;
        }
        ss = aux.front();

        cout << "Do you want to export for HTML or CSV? \n\t1 - HTML \n\t0 - CSV\n" << endl;
        cin >> opcao2;
        if (opcao2 == 1) {
            exhtml.createHTML();
        } else if (opcao2 == 0) {
            exc.exportCSV(ss);
        }
    }
}

