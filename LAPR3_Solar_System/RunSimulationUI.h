#ifndef RUNSIMULATIONUI_H
#define	RUNSIMULATIONUI_H

#include "RunSimulationController.h"

using namespace std;

class RunSimulationUI {
private:

    RunSimulationController runC;
    int resposta;

public:

    RunSimulationUI();
    ~RunSimulationUI();

    void run(SimulationList&);

};

#endif	/* RUNSIMULATIONUI_H */

