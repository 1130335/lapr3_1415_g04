#ifndef COMET_H
#define	COMET_H

#include <vector>
#include <string>
#include <iostream>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Comet : public NaturalObjects {
private:
    double ellipticalOrbit;

public:
    vector<Moon*> moons;
    Comet();
    Comet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    ~Comet();
    Comet(Comet&);
    void write(ostream& out);

    void setEllipticalOrbit(double eo);
    double getEllipticalOrbit() const;

    void addMoons(Moon*);
    void showMoons();
};

#endif	/* COMET_H */
