#ifndef COPYSOLARSYSTEMUI_H
#define	COPYSOLARSYSTEMUI_H

#include "CopySolarSystemController.h"
#include "SolarSystemList.h"
#include "SolarSystem.h"
#include <iostream>

using namespace std;

class CopySolarSystemUI {
    
private:
    CopySolarSystemController cssc;
public:
    CopySolarSystemUI();
    ~CopySolarSystemUI();
    
    void run(SolarSystemList&);
};

#endif	/* COPYSOLARSYSTEMUI_H */

