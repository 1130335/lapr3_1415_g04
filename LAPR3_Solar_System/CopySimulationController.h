#ifndef COPYSIMULATIONCONTROLLER_H
#define	COPYSIMULATIONCONTROLLER_H

#include "SimulationList.h"
#include "Simulation.h"
#include <iostream>

using namespace std;

class CopySimulationController {
public:
    CopySimulationController();
    virtual ~CopySimulationController();
    void showList(SimulationList&);
    Simulation* getSimulation(int, SimulationList&);
    void copySimulation(Simulation*, SimulationList&);
private:

};

#endif	/* COPYSIMULATIONCONTROLLER_H */

