#include "DeleteSimulationUI.h"
#include "DeleteSimulationController.h"

DeleteSimulationUI::DeleteSimulationUI() {
}

DeleteSimulationUI::~DeleteSimulationUI() {
}

void DeleteSimulationUI::run(SimulationList& sl) {

    Simulation* s = new Simulation();
    int resp;
    int op = 0;

    cout << "\nDELETE SIMULATION" << endl;
    
    
    do {
        cout << "Select the simulation you whish to delete from the following:" << endl;
        dsc.showList(sl);
        cout << "\nAnswer: ";
        cin >> op;
        while (op < 0 || op > sl.getSimulationList().size()) {
            cout << "\nYour answer was invalid... \n\n Answer:";
            cin >> op;
        }
        s = dsc.chooseSimulation(op, sl);
        cout << "Do you intend to delete the following simulation:" << endl;
        cout << s->getName();
        cout << "\nAnswer(1 - Y/0 - N): ";
        cin >> resp;
        while (resp > 1 || resp < 0) {
            cout << "\nYour answer was invalid... \n\n Answer(1 - Y/ 0 - N):";
            cin >> resp;
        }
    } while (resp != 1);
    dsc.deleteSimulation(op, sl);
    cout << "\n The simulation has been successfulfy deleted!" << endl;
}
