#ifndef MENUUI_H
#define	MENUUI_H

#include "CreateSolarSystemUI.h"
#include "CopySolarSystemUI.h"
#include "ImportSolarSystemUI.h"
#include "StartSimulationUI.h"
#include "OpenSolarSystemUI.h"
#include "EditSolarSystemUI.h"
#include "CopySimulationUI.h"
#include "SolarSystemList.h"
#include "SimulationList.h"
#include "RunSimulationUI.h"
#include "DeleteSimulationUI.h"
#include "ExportUI.h"
#include "EditSimulationUI.h"
#include <iostream>

using namespace std;

class MenuUI {

private:
    
    SolarSystemList ssl;
    SimulationList sl;
    CreateSolarSystemUI cssui;
    CopySolarSystemUI copyssui;
    ImportSolarSystemUI issui;
    OpenSolarSystemUI openui;
    EditSolarSystemUI editui;
    StartSimulationUI ssui;
    CopySimulationUI csui;
    RunSimulationUI runui;
    DeleteSimulationUI delui;
    ExportUI exui;
    EditSimulationUI esui;
    
public:
    MenuUI();
    ~MenuUI();
    void run();
};


#endif	/* MENUUI_H */

