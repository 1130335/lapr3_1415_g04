#ifndef STARTSIMULATIONCONTROLLER_H
#define	STARTSIMULATIONCONTROLLER_H

#include "Simulation.h"
#include "SolarSystem.h"
#include "Objects.h"
#include "NaturalObjects.h"
#include <string>
#include <list>
#include <stdlib.h> 

using namespace std;

class StartSimulationController {
private:
    Simulation s;
    NaturalObjects no;
    string reference;
    
public:

    StartSimulationController();
    virtual ~StartSimulationController();
    void setStart_time(double start_time);
    void setEnd_time(double end_time);
    void setTime_Step(double time_step);
    void setName(string);
    void setPosition(double, double, double);
    void setVelocity(double, double, double);
    void setSolarSystem(SolarSystem*);
    void showListNO();
    void addNO(int);
    void setReference(string);
    void setReference_time(int);
    void showListO();
    void addO(int);
    vector<string> splitDate(string);
    int referenceTime(int, vector<string>);
    Simulation& getSimulation();

};

#endif	/* STARTSIMULATIONCONTROLLER_H */

