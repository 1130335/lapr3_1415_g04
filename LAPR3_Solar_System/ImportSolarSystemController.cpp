#include "ImportSolarSystemController.h"

ImportSolarSystemController::ImportSolarSystemController() {
    ss = new SolarSystem();
}

ImportSolarSystemController::~ImportSolarSystemController() {

}

void ImportSolarSystemController::createStar(string n, double m, double ed) {
    Star s(n, m, ed);
    ss->setStar(s);
}

void ImportSolarSystemController::importSolarSystemCSV(string name, SolarSystemList& ssl) {
    int n = ssl.getSSL().size();
    stringstream s;
    ss = file_csv.importSolarSystem(name);
    s << "Solar_System_" << n + 1;
    string nS = s.str();
    ss->setName(nS);
}

void ImportSolarSystemController::showInfo(string name, double m, double ed) {
    createStar(name, m, ed);
    cout << endl << *ss;
}

void ImportSolarSystemController::addToSSL(SolarSystemList& ssl) {
    ssl.add(ss);
}
