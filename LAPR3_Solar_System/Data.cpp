#include "Data.h"

int Data::diasPorMes[]=	{0,31,28,31,30,31,30,31,31,30,31,30,31};

Data::Data() 
{
	setAno(1900);
	setMes(1);
	setDia(1);
}

Data::Data (int a, int m, int d ) 
{
	setAno(a);
	setMes(m);
	setDia(d);
}

Data::Data (const Data &d)
{
	ano=d.ano;
	mes=d.mes;
	dia=d.dia;
}

Data::~Data()
{

}

// Confirma o valor do dia baseado no mes e ano.
int Data::validaDia (int d) const
{
	if ( d > 0 && d <= diasPorMes[ mes ] ) 
		return d;

	if ( mes == 2 && d == 29 &&	anoBissexto(ano) ) 
		return d;
	cout<<"Dia " << d << " invalido. Colocado o dia 1.";
	    return 1;
}

int Data::validaMes (int m) const
{
	if (m > 0 && m <= 12 )		// valida o mes
	   return m;	
	else 
	{
	   cout<<"\nMês inválido -> mes=1";
	   return  1;
	}
}

int Data::validaAno (int a) const
{
	if (a < 0)
	{
	   cout<<"\nAno negativo - inválido -> ano=0";
	   return 0;
	}
	else
	   return a;
}

bool Data::anoBissexto(int a) const
{	
	return  ( a % 400 == 0 || a % 4 == 0 && a % 100 != 0 ) ;
}

void Data::setAno (int a)
{
	ano = validaAno (a) ;

}

void  Data::setMes (int m)
{
	mes = validaMes (m);

}

void  Data::setDia (int d)
{
	dia = validaDia (d);

}

void Data::setData (Data dt)
{
	setAno(dt.ano) ;  
	setMes(dt.mes);
	setDia(dt.dia);	 
}

int Data::getAno() const
{
	return ano;
}

int Data::getMes() const
{
	return mes;
}

int Data::getDia() const
{
	return dia;
}

int Data::getAnos() const
{
  time_t rawtime;
  struct tm * timeinfo;

  time ( &rawtime );
  timeinfo = localtime( &rawtime );
  
  if ((timeinfo->tm_mon+1) < mes) 
	  return ((timeinfo->tm_year+1900)-(ano+1)) ;
  else if ((timeinfo->tm_mon+1) == mes) 
	     if (timeinfo->tm_mday < dia)  
            return (timeinfo->tm_year+1900)-(ano+1) ; 
		 else
            return ((timeinfo->tm_year+1900)-ano) ;
  else
    return (timeinfo->tm_year+1900)-ano ;
}

void Data::listar() const 
{
	cout << dia << " / " << mes << " / " << ano << endl ;			 
}

