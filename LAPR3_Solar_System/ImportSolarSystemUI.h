#ifndef IMPORTSOLARSYSTEMUI_H
#define	IMPORTSOLARSYSTEMUI_H

#include "ImportSolarSystemController.h"
#include "SolarSystemList.h"
#include <iostream>
#include <string>

using namespace std;

class ImportSolarSystemUI {

private:
    ImportSolarSystemController issc;
    
public:
    ImportSolarSystemUI();
    ~ImportSolarSystemUI();
    
    void run(SolarSystemList&);
    
};

#endif	/* IMPORTSOLARSYSTEMUI_H */

