#ifndef CREATESSCONTROLLER_FT_H
#define	CREATESSCONTROLLER_FT_H

/* Teste Funcional à Classe Create Solar System Controller */

#include <iostream>

#include "SolarSystem.h"
#include "CreateSolarSystemController.h"
#include "SolarSystemList.h"
#include "Star.h"
#include "Planet.h"
#include "Satellite.h"

using namespace std;

class CreateSSController_FT {
private:
    SolarSystem* ss1;
    CreateSolarSystemController cssc;
    
public:
    CreateSSController_FT();
    ~CreateSSController_FT();
    
    void run();
};

#endif	/* CREATESSCONTROLLER_FT_H */

