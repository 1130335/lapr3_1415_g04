#include "CSV.h"

CSV::CSV() {

}

CSV::~CSV() {

}

void CSV::createCSV_SS(SolarSystem& ss) {
    stringstream s;
    s << ss.getName() << ".csv";
    string name = s.str();
    char * n = new char[name.length() + 1];
    strcpy(n, name.c_str());
    ofstream csv(n);
    csv << "Characteristics";
    list<NaturalObjects*> aux;
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getName();
        aux.pop_back();
    }
    csv << endl;

    csv << "Orbiting";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getOrbiting();
        aux.pop_back();
    }
    csv << endl;

    csv << "Period of revolution (years)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getPeriodRevolution();
        aux.pop_back();
    }
    csv << endl;

    csv << "Orbital speed m/s";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getOrbitalSpeed();
        aux.pop_back();
    }
    csv << endl;

    csv << "Inclination of axis to orbit";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getInclinationAxisOrbit();
        aux.pop_back();
    }
    csv << endl;

    csv << "Equatorial diameter (km)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getEquatorialDiameter();
        aux.pop_back();
    }
    csv << endl;

    csv << "Mass";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getMass();
        aux.pop_back();
    }

    csv << endl;
    csv << "Density (water = 1)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getDensity();
        aux.pop_back();
    }
    csv << endl;

    csv << "Escape velocity, equator (km/s)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getEscapeVelocity();
        aux.pop_back();
    }
    csv << endl;

    csv << "Semimajor Axis (a) (km)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getSemiMajorAxis();
        aux.pop_back();
    }
    csv << endl;

    csv << "Orbit Eccentricity ( e)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getEccentricity();
        aux.pop_back();
    }
    csv << endl;

    csv << "Orbit Inclination (i) degrees";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getOrbitInclination();
        aux.pop_back();
    }
    csv << endl;

    csv << "Mean anomaly J2000 (M) rad";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getMeanAnomaly();
        aux.pop_back();
    }
    csv << endl;

    csv << "Longitude of perihelion (w) degrees";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getLongitudeP();
        aux.pop_back();
    }
    csv << endl;

    csv << "Longitude of ascending node (omega) degrees";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getLongitudeAsc();
        aux.pop_back();
    }
    csv << endl;

    csv << "Perihelion (km)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getPerihelion();
        aux.pop_back();
    }
    csv << endl;

    csv << "Aphelion (km)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getAphelion();
        aux.pop_back();
    }
    csv << endl;

    csv << "Px (AU)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getPx();
        aux.pop_back();
    }
    csv << endl;

    csv << "Py (AU)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getPy();
        aux.pop_back();
    }
    csv << endl;

    csv << "Pz (AU)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getPz();
        aux.pop_back();
    }
    csv << endl;

    csv << "Vx (m/s)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getVx();
        aux.pop_back();
    }
    csv << endl;

    csv << "Vy (m/s)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getVy();
        aux.pop_back();
    }
    csv << endl;

    csv << "Vz (m/s)";
    aux = ss.getNObj();
    while (!aux.empty()) {
        csv << ";" << aux.back()->getVz();
        aux.pop_back();
    }
    csv << endl;

    csv << "Reference Date;";
    csv << endl;

    csv.close();
}

SolarSystem* CSV::importSolarSystem(string name) {
    list<string>* aux;
    list<list<string>* > data;
    vector<NaturalObjects*> nObj;

    SolarSystem* ss = new SolarSystem();
    char * nFile = new char[name.length() + 1];
    strcpy(nFile, name.c_str());
    ifstream csv(nFile);
    if (!csv) {
        cout << "\nThere is no file with such name.\n";
    }

    string word;
    size_t pos = 0;
    string token;
    string delimiter = ";";
    while (getline(csv, word)) {
        aux = new list<string>();
        while ((pos = word.find(delimiter)) != string::npos) {
            token = word.substr(0, pos);
            aux->push_front(token);
            word.erase(0, pos + delimiter.length());
        }
        aux->push_front(word);
        data.push_front(aux);
    }

    if (data.size() == 24) {
        type3File(data, ss);
    } else {
        list<list<string>* > aux1 = data;
        aux = aux1.back();
        int nElem = aux->size() - 1;

        for (int i = 0; i < nElem; i++) {
            nObj.push_back(new NaturalObjects());
        }

        aux1 = data;
        aux = aux1.back();
        aux->pop_back();

        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setName(aux->back());
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        if (aux->back() == "Orbiting") {
            aux->pop_back();
            for (int i = 0; i < nElem; i++) {
                (nObj[i])->setOrbiting(aux->back());
                aux->pop_back();
            }
            aux1.pop_back();
            aux = aux1.back();
        }

        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setPeriodRevolution(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setOrbitalSpeed(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setInclinationAxisOrbit(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setEquatorialDiameter(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setMass(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setDensity(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setEscapeVelocity(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setSemiMajorAxis(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setEccentricity(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setOrbitInclination(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setPerihelion(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setAphelion(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setPx(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setPy(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setPz(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setVx(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setVy(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setVz(atof(aux->back().c_str()));
            aux->pop_back();
        }

        aux1.pop_back();
        aux = aux1.back();
        aux->pop_back();
        for (int i = 0; i < nElem; i++) {
            (nObj[i])->setReference(aux->back());
            aux->pop_back();
        }

        Planet* p;
        Moon* m;
        for (int i = 0; i < nElem; i++) {
            if (nObj[i]->getOrbiting() == "Star") {
                p = new Planet();
                p->setName(nObj[i]->getName());
                p->setAphelion(nObj[i]->getAphelion());
                p->setDensity(nObj[i]->getDensity());
                p->setEccentricity(nObj[i]->getEccentricity());
                p->setEquatorialDiameter(nObj[i]->getEquatorialDiameter());
                p->setEscapeVelocity(nObj[i]->getEscapeVelocity());
                p->setInclinationAxisOrbit(nObj[i]->getInclinationAxisOrbit());
                p->setMass(nObj[i]->getMass());
                p->setOrbitalSpeed(nObj[i]->getOrbitalSpeed());
                p->setOrbitInclination(nObj[i]->getOrbitInclination());
                p->setPerihelion(nObj[i]->getPerihelion());
                p->setPeriodRevolution(nObj[i]->getPeriodRevolution());
                p->setPx(nObj[i]->getPx());
                p->setPy(nObj[i]->getPy());
                p->setPz(nObj[i]->getPz());
                p->setSemiMajorAxis(nObj[i]->getSemiMajorAxis());
                p->setVx(nObj[i]->getVx());
                p->setVy(nObj[i]->getVy());
                p->setVz(nObj[i]->getVz());
                p->setReference(nObj[i]->getReference());
                ss->addToNaturalObj(p);
            } else {
                m = new Moon();
                m->setName(nObj[i]->getName());
                m->setAphelion(nObj[i]->getAphelion());
                m->setDensity(nObj[i]->getDensity());
                m->setEccentricity(nObj[i]->getEccentricity());
                m->setEquatorialDiameter(nObj[i]->getEquatorialDiameter());
                m->setEscapeVelocity(nObj[i]->getEscapeVelocity());
                m->setInclinationAxisOrbit(nObj[i]->getInclinationAxisOrbit());
                m->setMass(nObj[i]->getMass());
                m->setOrbitalSpeed(nObj[i]->getOrbitalSpeed());
                m->setOrbitInclination(nObj[i]->getOrbitInclination());
                m->setPerihelion(nObj[i]->getPerihelion());
                m->setPeriodRevolution(nObj[i]->getPeriodRevolution());
                m->setPx(nObj[i]->getPx());
                m->setPy(nObj[i]->getPy());
                m->setPz(nObj[i]->getPz());
                m->setSemiMajorAxis(nObj[i]->getSemiMajorAxis());
                m->setVx(nObj[i]->getVx());
                m->setVy(nObj[i]->getVy());
                m->setVz(nObj[i]->getVz());
                m->setOrbiting(nObj[i]->getOrbiting());
                m->setReference(nObj[i]->getReference());
                ss->addMoontoBody(m);
            }
        }
    }

    csv.close();

    return ss;
}

void CSV::type3File(list<list<string>* > data, SolarSystem* ss) {
    list<string>* aux;
    vector<NaturalObjects*> nObj;

    list<list<string>* > aux1 = data;
    aux = aux1.back();
    int nElem = aux->size() - 1;

    for (int i = 0; i < nElem; i++) {
        nObj.push_back(new NaturalObjects());
    }

    aux1 = data;
    aux = aux1.back();
    aux->pop_back();

    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setName(aux->back());
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setOrbiting(aux->back());
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setPeriodRevolution(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setOrbitalSpeed(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setInclinationAxisOrbit(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setEquatorialDiameter(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setMass(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setDensity(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setEscapeVelocity(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setSemiMajorAxis(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setEccentricity(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setOrbitInclination(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setMeanAnomaly(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setLongitudeP(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setLongitudeAsc(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setPerihelion(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setAphelion(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setPx(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setPy(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setPz(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setVx(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setVy(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setVz(atof(aux->back().c_str()));
        aux->pop_back();
    }

    aux1.pop_back();
    aux = aux1.back();
    aux->pop_back();
    for (int i = 0; i < nElem; i++) {
        (nObj[i])->setReference(aux->back());
        aux->pop_back();
    }

    Planet* p;
    for (int i = 0; i < nElem; i++) {
        p = new Planet();
        p->setName(nObj[i]->getName());
        p->setAphelion(nObj[i]->getAphelion());
        p->setDensity(nObj[i]->getDensity());
        p->setEccentricity(nObj[i]->getEccentricity());
        p->setEquatorialDiameter(nObj[i]->getEquatorialDiameter());
        p->setEscapeVelocity(nObj[i]->getEscapeVelocity());
        p->setInclinationAxisOrbit(nObj[i]->getInclinationAxisOrbit());
        p->setMass(nObj[i]->getMass());
        p->setOrbitalSpeed(nObj[i]->getOrbitalSpeed());
        p->setOrbitInclination(nObj[i]->getOrbitInclination());
        p->setPerihelion(nObj[i]->getPerihelion());
        p->setPeriodRevolution(nObj[i]->getPeriodRevolution());
        p->setPx(nObj[i]->getPx());
        p->setPy(nObj[i]->getPy());
        p->setPz(nObj[i]->getPz());
        p->setSemiMajorAxis(nObj[i]->getSemiMajorAxis());
        p->setVx(nObj[i]->getVx());
        p->setVy(nObj[i]->getVy());
        p->setVz(nObj[i]->getVz());
        p->setMeanAnomaly(nObj[i]->getMeanAnomaly());
        p->setLongitudeP(nObj[i]->getLongitudeP());
        p->setLongitudeAsc(nObj[i]->getLongitudeAsc());
        p->setReference(nObj[i]->getReference());
        ss->addToNaturalObj(p);
    }
}

