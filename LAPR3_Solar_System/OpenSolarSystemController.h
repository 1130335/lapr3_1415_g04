/* 
 * File:   OpenSolarSystemController.h
 * Author: Asus
 *
 * Created on 17 de Dezembro de 2014, 17:32
 */

#ifndef OPENSOLARSYSTEMCONTROLLER_H
#define	OPENSOLARSYSTEMCONTROLLER_H

#include <list>
#include "SolarSystem.h"
#include "SolarSystemList.h"

using namespace std;

class OpenSolarSystemController {

private:
    list<SolarSystem*> lss;
    
public:
    OpenSolarSystemController();
    ~OpenSolarSystemController();
    
    int alterListaSistemasSolares();
    SolarSystem* getInfoSS(int);
    
    list<SolarSystem*> getLSS() const;
    void setLSS(list<SolarSystem*> listaSS);
    
};

#endif	/* OPENSOLARSYSTEMCONTROLLER_H */

