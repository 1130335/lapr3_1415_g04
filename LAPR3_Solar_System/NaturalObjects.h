#ifndef NATURALOBJECTS_H
#define	NATURALOBJECTS_H

#include <string>
#include <iostream>
#include <vector>

using namespace std;

class NaturalObjects {
private:
    string name;
    double Px; // AU
    double Py; // AU
    double Pz; // AU
    double Vx; // m/s
    double Vy; // m/s
    double Vz; // m/s
    double period_revolution; // years
    double orbital_speed; // m/s
    double inclination_axis_orbit;
    double semimajor_axis; // (a) km
    double eccentricity; // (e)
    double orbit_inclination; // (i) degrees
    double equatorial_diameter; // km
    double mass;
    double density; // water = 1
    double escape_velocity; // km/s
    double perihelion; // km
    double aphelion; // km
    double mean_anomaly;
    double long_perihelion;
    double long_ascending;
    string orbiting;
    string reference;
    vector<double> position;
    vector<double> velocity;

public:
    NaturalObjects();
    NaturalObjects(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    NaturalObjects(NaturalObjects&);
    virtual ~NaturalObjects();

    string getName();
    double getPx();
    double getPy();
    double getPz();
    double getVx();
    double getVy();
    double getVz();
    double getPeriodRevolution();
    double getOrbitalSpeed();
    double getInclinationAxisOrbit();
    double getSemiMajorAxis();
    double getEccentricity();
    double getOrbitInclination();
    double getEquatorialDiameter();
    double getMass();
    double getDensity();
    double getEscapeVelocity();
    double getPerihelion();
    double getAphelion();
    string getOrbiting();
    double getMeanAnomaly();
    double getLongitudeP();
    double getLongitudeAsc();
    string getReference();
    vector<double> getPosition();
    vector<double> getVelocity();
   
    void setPosition(double, double, double);
    void setVelocity(double, double, double);
    void setName(string);
    void setPx(double);
    void setPy(double);
    void setPz(double);
    void setVx(double);
    void setVy(double);
    void setVz(double);
    void setPeriodRevolution(double);
    void setOrbitalSpeed(double);
    void setInclinationAxisOrbit(double);
    void setSemiMajorAxis(double);
    void setEccentricity(double);
    void setOrbitInclination(double);
    void setEquatorialDiameter(double);
    void setMass(double);
    void setDensity(double);
    void setEscapeVelocity(double);
    void setPerihelion(double);
    void setAphelion(double);
    void setOrbiting(string);
    void setMeanAnomaly(double);
    void setLongitudeP(double);
    void setLongitudeAsc(double);
    void setReference(string);
    
    virtual void write(ostream&);
};

ostream & operator<<(ostream& out, NaturalObjects& no);

#endif	/* NATURALOBJECTS_H */

