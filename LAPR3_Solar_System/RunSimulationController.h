#ifndef RUNSIMULATIONCONTROLLER_H
#define	RUNSIMULATIONCONTROLLER_H

#include <math.h>
#include <string>
#include "SolarSystem.h"
#include "Simulation.h"
#include "SimulationList.h"
#include "VerletAlgorithm.h"
#include "Force.h"

using namespace std;

class RunSimulationController {

public:

    RunSimulationController();
    RunSimulationController(string, double, double, double, double, double, double, double, SolarSystem&);
    RunSimulationController(RunSimulationController&);
    ~RunSimulationController();
    
    void runVerlet(Simulation*);
    vector<double> getMass(vector<NaturalObjects*>);
    list<Simulation*> getSimulationList(SimulationList&);
    void showList(SimulationList& sl);
    Simulation* chooseSimulation(int, SimulationList&);

    void setName(string name);
    void setForce(double force);
    void setM1(double m1);
    void setM2(double m2);
    void setDistance(double distance);
    void setUnitVector(double unitVector);
    void setVelocity(double velocity);

    string getName() const;
    double getForce() const;
    double getGravitacionalConstant() const;
    double getM1() const;
    double getM2() const;
    double getDistance() const;
    double getUnitVector() const;
    double getVelocity() const;
    SolarSystem getSolar() const;

    void write(ostream& out) const;

private:

    string name;
    double force;
    double G;
    double m1;
    double m2;
    double distance;
    double unitVector;
    double velocity;
    SolarSystem solar;

};

#endif	/* RUNSIMULATIONCONTROLLER_H */

