#ifndef DELETESIMULATIONCONTROLLER_H
#define	DELETESIMULATIONCONTROLLER_H

#include <list>
#include <string>
#include <iostream>
#include "SimulationList.h"
#include "Simulation.h"

class DeleteSimulationController {

public:
    
    DeleteSimulationController();
    DeleteSimulationController(const DeleteSimulationController& orig);
    ~DeleteSimulationController();
    
    list<Simulation*> getSimulationList(SimulationList&);
    void showList(SimulationList&);
    Simulation* chooseSimulation(int,SimulationList&);
    void deleteSimulation(int, SimulationList&);
    
private:

};

#endif	/* DELETESIMULATIONCONTROLLER_H */

