#ifndef OBJECTS_H
#define	OBJECTS_H

#include <string>
#include <iostream>

using namespace std;

class Objects {
private:
    double velocity;
    double mass;
    double graForce;
    double orbit;
    int id;

public:
    Objects();
    Objects(double v, double m, double g, double o, int id);
    Objects(const Objects& ob);
    virtual ~Objects();
    virtual void write(ostream& out) const;


    void setVelocity(double v);
    void setMass(double m);
    void setGraForce(double f);
    void setOrbit(double o);
    void setID(int id);

    double getVelocity() const;
    double getMass() const;
    double getGraForce() const;
    double getOrbit() const;
    double getID()const;
};

ostream & operator<<(ostream &out, Objects& o);

#endif	/* Objects_ */
