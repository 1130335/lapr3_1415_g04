#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Asteroid.o \
	${OBJECTDIR}/BDados.o \
	${OBJECTDIR}/Body.o \
	${OBJECTDIR}/Bolide.o \
	${OBJECTDIR}/CSV.o \
	${OBJECTDIR}/Comet.o \
	${OBJECTDIR}/CopySSController_FT.o \
	${OBJECTDIR}/CopySimulationController.o \
	${OBJECTDIR}/CopySimulationUI.o \
	${OBJECTDIR}/CopySolarSystemController.o \
	${OBJECTDIR}/CopySolarSystemUI.o \
	${OBJECTDIR}/CreateSSController_FT.o \
	${OBJECTDIR}/CreateSolarSystemController.o \
	${OBJECTDIR}/CreateSolarSystemUI.o \
	${OBJECTDIR}/DeleteSimulationController.o \
	${OBJECTDIR}/DeleteSimulationUI.o \
	${OBJECTDIR}/EditSimulationController.o \
	${OBJECTDIR}/EditSimulationUI.o \
	${OBJECTDIR}/EditSolarSystemController.o \
	${OBJECTDIR}/EditSolarSystemUI.o \
	${OBJECTDIR}/ExportController.o \
	${OBJECTDIR}/ExportHTML.o \
	${OBJECTDIR}/ExportUI.o \
	${OBJECTDIR}/Fireball.o \
	${OBJECTDIR}/Force.o \
	${OBJECTDIR}/ImportSolarSystemController.o \
	${OBJECTDIR}/ImportSolarSystemUI.o \
	${OBJECTDIR}/MenuUI.o \
	${OBJECTDIR}/Meteor.o \
	${OBJECTDIR}/Meteorite.o \
	${OBJECTDIR}/Meteoroid.o \
	${OBJECTDIR}/MinorPlanet.o \
	${OBJECTDIR}/Moon.o \
	${OBJECTDIR}/NaturalObjects.o \
	${OBJECTDIR}/NewtonLaws.o \
	${OBJECTDIR}/Objects.o \
	${OBJECTDIR}/OpenSolarSystemController.o \
	${OBJECTDIR}/OpenSolarSystemUI.o \
	${OBJECTDIR}/Planet.o \
	${OBJECTDIR}/Results.o \
	${OBJECTDIR}/RunSimulationController.o \
	${OBJECTDIR}/RunSimulationUI.o \
	${OBJECTDIR}/Satellite.o \
	${OBJECTDIR}/Simulation.o \
	${OBJECTDIR}/SimulationList.o \
	${OBJECTDIR}/SolarSystem.o \
	${OBJECTDIR}/SolarSystemList.o \
	${OBJECTDIR}/SpaceShip.o \
	${OBJECTDIR}/Star.o \
	${OBJECTDIR}/StartSimulationController.o \
	${OBJECTDIR}/StartSimulationUI.o \
	${OBJECTDIR}/VerletAlgorithm.o \
	${OBJECTDIR}/main.o

# Test Directory
TESTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}/tests

# Test Files
TESTFILES= \
	${TESTDIR}/TestFiles/f1 \
	${TESTDIR}/TestFiles/f2 \
	${TESTDIR}/TestFiles/f5 \
	${TESTDIR}/TestFiles/f4 \
	${TESTDIR}/TestFiles/f3

# C Compiler Flags
CFLAGS=`cppunit-config --cflags` 

# CC Compiler Flags
CCFLAGS=-std=c++11
CXXFLAGS=-std=c++11

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=`cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs` `cppunit-config --libs`  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${TESTDIR}/TestFiles/f2.exe

${TESTDIR}/TestFiles/f2.exe: ${OBJECTFILES}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc} -o ${TESTDIR}/TestFiles/f2 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/Asteroid.o: Asteroid.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Asteroid.o Asteroid.cpp

${OBJECTDIR}/BDados.o: BDados.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BDados.o BDados.cpp

${OBJECTDIR}/Body.o: Body.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Body.o Body.cpp

${OBJECTDIR}/Bolide.o: Bolide.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bolide.o Bolide.cpp

${OBJECTDIR}/CSV.o: CSV.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CSV.o CSV.cpp

${OBJECTDIR}/Comet.o: Comet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Comet.o Comet.cpp

${OBJECTDIR}/CopySSController_FT.o: CopySSController_FT.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySSController_FT.o CopySSController_FT.cpp

${OBJECTDIR}/CopySimulationController.o: CopySimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySimulationController.o CopySimulationController.cpp

${OBJECTDIR}/CopySimulationUI.o: CopySimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySimulationUI.o CopySimulationUI.cpp

${OBJECTDIR}/CopySolarSystemController.o: CopySolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySolarSystemController.o CopySolarSystemController.cpp

${OBJECTDIR}/CopySolarSystemUI.o: CopySolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySolarSystemUI.o CopySolarSystemUI.cpp

${OBJECTDIR}/CreateSSController_FT.o: CreateSSController_FT.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSSController_FT.o CreateSSController_FT.cpp

${OBJECTDIR}/CreateSolarSystemController.o: CreateSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSolarSystemController.o CreateSolarSystemController.cpp

${OBJECTDIR}/CreateSolarSystemUI.o: CreateSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSolarSystemUI.o CreateSolarSystemUI.cpp

${OBJECTDIR}/DeleteSimulationController.o: DeleteSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DeleteSimulationController.o DeleteSimulationController.cpp

${OBJECTDIR}/DeleteSimulationUI.o: DeleteSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DeleteSimulationUI.o DeleteSimulationUI.cpp

${OBJECTDIR}/EditSimulationController.o: EditSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSimulationController.o EditSimulationController.cpp

${OBJECTDIR}/EditSimulationUI.o: EditSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSimulationUI.o EditSimulationUI.cpp

${OBJECTDIR}/EditSolarSystemController.o: EditSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSolarSystemController.o EditSolarSystemController.cpp

${OBJECTDIR}/EditSolarSystemUI.o: EditSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSolarSystemUI.o EditSolarSystemUI.cpp

${OBJECTDIR}/ExportController.o: ExportController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportController.o ExportController.cpp

${OBJECTDIR}/ExportHTML.o: ExportHTML.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportHTML.o ExportHTML.cpp

${OBJECTDIR}/ExportUI.o: ExportUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportUI.o ExportUI.cpp

${OBJECTDIR}/Fireball.o: Fireball.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Fireball.o Fireball.cpp

${OBJECTDIR}/Force.o: Force.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Force.o Force.cpp

${OBJECTDIR}/ImportSolarSystemController.o: ImportSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImportSolarSystemController.o ImportSolarSystemController.cpp

${OBJECTDIR}/ImportSolarSystemUI.o: ImportSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImportSolarSystemUI.o ImportSolarSystemUI.cpp

${OBJECTDIR}/MenuUI.o: MenuUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MenuUI.o MenuUI.cpp

${OBJECTDIR}/Meteor.o: Meteor.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteor.o Meteor.cpp

${OBJECTDIR}/Meteorite.o: Meteorite.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteorite.o Meteorite.cpp

${OBJECTDIR}/Meteoroid.o: Meteoroid.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteoroid.o Meteoroid.cpp

${OBJECTDIR}/MinorPlanet.o: MinorPlanet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MinorPlanet.o MinorPlanet.cpp

${OBJECTDIR}/Moon.o: Moon.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Moon.o Moon.cpp

${OBJECTDIR}/NaturalObjects.o: NaturalObjects.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NaturalObjects.o NaturalObjects.cpp

${OBJECTDIR}/NewtonLaws.o: NewtonLaws.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NewtonLaws.o NewtonLaws.cpp

${OBJECTDIR}/Objects.o: Objects.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Objects.o Objects.cpp

${OBJECTDIR}/OpenSolarSystemController.o: OpenSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OpenSolarSystemController.o OpenSolarSystemController.cpp

${OBJECTDIR}/OpenSolarSystemUI.o: OpenSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OpenSolarSystemUI.o OpenSolarSystemUI.cpp

${OBJECTDIR}/Planet.o: Planet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Planet.o Planet.cpp

${OBJECTDIR}/Results.o: Results.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Results.o Results.cpp

${OBJECTDIR}/RunSimulationController.o: RunSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RunSimulationController.o RunSimulationController.cpp

${OBJECTDIR}/RunSimulationUI.o: RunSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RunSimulationUI.o RunSimulationUI.cpp

${OBJECTDIR}/Satellite.o: Satellite.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Satellite.o Satellite.cpp

${OBJECTDIR}/Simulation.o: Simulation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Simulation.o Simulation.cpp

${OBJECTDIR}/SimulationList.o: SimulationList.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SimulationList.o SimulationList.cpp

${OBJECTDIR}/SolarSystem.o: SolarSystem.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SolarSystem.o SolarSystem.cpp

${OBJECTDIR}/SolarSystemList.o: SolarSystemList.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SolarSystemList.o SolarSystemList.cpp

${OBJECTDIR}/SpaceShip.o: SpaceShip.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SpaceShip.o SpaceShip.cpp

${OBJECTDIR}/Star.o: Star.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Star.o Star.cpp

${OBJECTDIR}/StartSimulationController.o: StartSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StartSimulationController.o StartSimulationController.cpp

${OBJECTDIR}/StartSimulationUI.o: StartSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StartSimulationUI.o StartSimulationUI.cpp

${OBJECTDIR}/VerletAlgorithm.o: VerletAlgorithm.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/VerletAlgorithm.o VerletAlgorithm.cpp

${OBJECTDIR}/main.o: main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:

# Build Test Targets
.build-tests-conf: .build-conf ${TESTFILES}
${TESTDIR}/TestFiles/f1: ${TESTDIR}/tests/NOT_runner.o ${TESTDIR}/tests/NaturalObjects_Test.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f1 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f2: ${TESTDIR}/tests/OT_runner.o ${TESTDIR}/tests/Objects_Test.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f2 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f5: ${TESTDIR}/tests/Simulation_Test.o ${TESTDIR}/tests/Simulation_runner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f5 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f4: ${TESTDIR}/tests/SolarSystem_Test.o ${TESTDIR}/tests/SolarSystem_runner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f4 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   

${TESTDIR}/TestFiles/f3: ${TESTDIR}/tests/Star_Test.o ${TESTDIR}/tests/Star_runner.o ${OBJECTFILES:%.o=%_nomain.o}
	${MKDIR} -p ${TESTDIR}/TestFiles
	${LINK.cc}   -o ${TESTDIR}/TestFiles/f3 $^ ${LDLIBSOPTIONS} `cppunit-config --libs`   


${TESTDIR}/tests/NOT_runner.o: tests/NOT_runner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/NOT_runner.o tests/NOT_runner.cpp


${TESTDIR}/tests/NaturalObjects_Test.o: tests/NaturalObjects_Test.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/NaturalObjects_Test.o tests/NaturalObjects_Test.cpp


${TESTDIR}/tests/OT_runner.o: tests/OT_runner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/OT_runner.o tests/OT_runner.cpp


${TESTDIR}/tests/Objects_Test.o: tests/Objects_Test.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Objects_Test.o tests/Objects_Test.cpp


${TESTDIR}/tests/Simulation_Test.o: tests/Simulation_Test.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Simulation_Test.o tests/Simulation_Test.cpp


${TESTDIR}/tests/Simulation_runner.o: tests/Simulation_runner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Simulation_runner.o tests/Simulation_runner.cpp


${TESTDIR}/tests/SolarSystem_Test.o: tests/SolarSystem_Test.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/SolarSystem_Test.o tests/SolarSystem_Test.cpp


${TESTDIR}/tests/SolarSystem_runner.o: tests/SolarSystem_runner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/SolarSystem_runner.o tests/SolarSystem_runner.cpp


${TESTDIR}/tests/Star_Test.o: tests/Star_Test.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Star_Test.o tests/Star_Test.cpp


${TESTDIR}/tests/Star_runner.o: tests/Star_runner.cpp 
	${MKDIR} -p ${TESTDIR}/tests
	${RM} "$@.d"
	$(COMPILE.cc) -g `cppunit-config --cflags` -MMD -MP -MF "$@.d" -o ${TESTDIR}/tests/Star_runner.o tests/Star_runner.cpp


${OBJECTDIR}/Asteroid_nomain.o: ${OBJECTDIR}/Asteroid.o Asteroid.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Asteroid.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Asteroid_nomain.o Asteroid.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Asteroid.o ${OBJECTDIR}/Asteroid_nomain.o;\
	fi

${OBJECTDIR}/BDados_nomain.o: ${OBJECTDIR}/BDados.o BDados.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/BDados.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/BDados_nomain.o BDados.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/BDados.o ${OBJECTDIR}/BDados_nomain.o;\
	fi

${OBJECTDIR}/Body_nomain.o: ${OBJECTDIR}/Body.o Body.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Body.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Body_nomain.o Body.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Body.o ${OBJECTDIR}/Body_nomain.o;\
	fi

${OBJECTDIR}/Bolide_nomain.o: ${OBJECTDIR}/Bolide.o Bolide.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Bolide.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Bolide_nomain.o Bolide.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Bolide.o ${OBJECTDIR}/Bolide_nomain.o;\
	fi

${OBJECTDIR}/CSV_nomain.o: ${OBJECTDIR}/CSV.o CSV.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CSV.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CSV_nomain.o CSV.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CSV.o ${OBJECTDIR}/CSV_nomain.o;\
	fi

${OBJECTDIR}/Comet_nomain.o: ${OBJECTDIR}/Comet.o Comet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Comet.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Comet_nomain.o Comet.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Comet.o ${OBJECTDIR}/Comet_nomain.o;\
	fi

${OBJECTDIR}/CopySSController_FT_nomain.o: ${OBJECTDIR}/CopySSController_FT.o CopySSController_FT.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CopySSController_FT.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySSController_FT_nomain.o CopySSController_FT.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CopySSController_FT.o ${OBJECTDIR}/CopySSController_FT_nomain.o;\
	fi

${OBJECTDIR}/CopySimulationController_nomain.o: ${OBJECTDIR}/CopySimulationController.o CopySimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CopySimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySimulationController_nomain.o CopySimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CopySimulationController.o ${OBJECTDIR}/CopySimulationController_nomain.o;\
	fi

${OBJECTDIR}/CopySimulationUI_nomain.o: ${OBJECTDIR}/CopySimulationUI.o CopySimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CopySimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySimulationUI_nomain.o CopySimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CopySimulationUI.o ${OBJECTDIR}/CopySimulationUI_nomain.o;\
	fi

${OBJECTDIR}/CopySolarSystemController_nomain.o: ${OBJECTDIR}/CopySolarSystemController.o CopySolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CopySolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySolarSystemController_nomain.o CopySolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CopySolarSystemController.o ${OBJECTDIR}/CopySolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/CopySolarSystemUI_nomain.o: ${OBJECTDIR}/CopySolarSystemUI.o CopySolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CopySolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CopySolarSystemUI_nomain.o CopySolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CopySolarSystemUI.o ${OBJECTDIR}/CopySolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/CreateSSController_FT_nomain.o: ${OBJECTDIR}/CreateSSController_FT.o CreateSSController_FT.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CreateSSController_FT.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSSController_FT_nomain.o CreateSSController_FT.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CreateSSController_FT.o ${OBJECTDIR}/CreateSSController_FT_nomain.o;\
	fi

${OBJECTDIR}/CreateSolarSystemController_nomain.o: ${OBJECTDIR}/CreateSolarSystemController.o CreateSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CreateSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSolarSystemController_nomain.o CreateSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CreateSolarSystemController.o ${OBJECTDIR}/CreateSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/CreateSolarSystemUI_nomain.o: ${OBJECTDIR}/CreateSolarSystemUI.o CreateSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/CreateSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/CreateSolarSystemUI_nomain.o CreateSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/CreateSolarSystemUI.o ${OBJECTDIR}/CreateSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/DeleteSimulationController_nomain.o: ${OBJECTDIR}/DeleteSimulationController.o DeleteSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/DeleteSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DeleteSimulationController_nomain.o DeleteSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/DeleteSimulationController.o ${OBJECTDIR}/DeleteSimulationController_nomain.o;\
	fi

${OBJECTDIR}/DeleteSimulationUI_nomain.o: ${OBJECTDIR}/DeleteSimulationUI.o DeleteSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/DeleteSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DeleteSimulationUI_nomain.o DeleteSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/DeleteSimulationUI.o ${OBJECTDIR}/DeleteSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/EditSimulationController_nomain.o: ${OBJECTDIR}/EditSimulationController.o EditSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/EditSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSimulationController_nomain.o EditSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/EditSimulationController.o ${OBJECTDIR}/EditSimulationController_nomain.o;\
	fi

${OBJECTDIR}/EditSimulationUI_nomain.o: ${OBJECTDIR}/EditSimulationUI.o EditSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/EditSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSimulationUI_nomain.o EditSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/EditSimulationUI.o ${OBJECTDIR}/EditSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/EditSolarSystemController_nomain.o: ${OBJECTDIR}/EditSolarSystemController.o EditSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/EditSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSolarSystemController_nomain.o EditSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/EditSolarSystemController.o ${OBJECTDIR}/EditSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/EditSolarSystemUI_nomain.o: ${OBJECTDIR}/EditSolarSystemUI.o EditSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/EditSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/EditSolarSystemUI_nomain.o EditSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/EditSolarSystemUI.o ${OBJECTDIR}/EditSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/ExportController_nomain.o: ${OBJECTDIR}/ExportController.o ExportController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ExportController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportController_nomain.o ExportController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ExportController.o ${OBJECTDIR}/ExportController_nomain.o;\
	fi

${OBJECTDIR}/ExportHTML_nomain.o: ${OBJECTDIR}/ExportHTML.o ExportHTML.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ExportHTML.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportHTML_nomain.o ExportHTML.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ExportHTML.o ${OBJECTDIR}/ExportHTML_nomain.o;\
	fi

${OBJECTDIR}/ExportUI_nomain.o: ${OBJECTDIR}/ExportUI.o ExportUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ExportUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ExportUI_nomain.o ExportUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ExportUI.o ${OBJECTDIR}/ExportUI_nomain.o;\
	fi

${OBJECTDIR}/Fireball_nomain.o: ${OBJECTDIR}/Fireball.o Fireball.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Fireball.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Fireball_nomain.o Fireball.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Fireball.o ${OBJECTDIR}/Fireball_nomain.o;\
	fi

${OBJECTDIR}/Force_nomain.o: ${OBJECTDIR}/Force.o Force.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Force.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Force_nomain.o Force.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Force.o ${OBJECTDIR}/Force_nomain.o;\
	fi

${OBJECTDIR}/ImportSolarSystemController_nomain.o: ${OBJECTDIR}/ImportSolarSystemController.o ImportSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ImportSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImportSolarSystemController_nomain.o ImportSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ImportSolarSystemController.o ${OBJECTDIR}/ImportSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/ImportSolarSystemUI_nomain.o: ${OBJECTDIR}/ImportSolarSystemUI.o ImportSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/ImportSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ImportSolarSystemUI_nomain.o ImportSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/ImportSolarSystemUI.o ${OBJECTDIR}/ImportSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/MenuUI_nomain.o: ${OBJECTDIR}/MenuUI.o MenuUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MenuUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MenuUI_nomain.o MenuUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MenuUI.o ${OBJECTDIR}/MenuUI_nomain.o;\
	fi

${OBJECTDIR}/Meteor_nomain.o: ${OBJECTDIR}/Meteor.o Meteor.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Meteor.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteor_nomain.o Meteor.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Meteor.o ${OBJECTDIR}/Meteor_nomain.o;\
	fi

${OBJECTDIR}/Meteorite_nomain.o: ${OBJECTDIR}/Meteorite.o Meteorite.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Meteorite.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteorite_nomain.o Meteorite.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Meteorite.o ${OBJECTDIR}/Meteorite_nomain.o;\
	fi

${OBJECTDIR}/Meteoroid_nomain.o: ${OBJECTDIR}/Meteoroid.o Meteoroid.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Meteoroid.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Meteoroid_nomain.o Meteoroid.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Meteoroid.o ${OBJECTDIR}/Meteoroid_nomain.o;\
	fi

${OBJECTDIR}/MinorPlanet_nomain.o: ${OBJECTDIR}/MinorPlanet.o MinorPlanet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/MinorPlanet.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MinorPlanet_nomain.o MinorPlanet.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/MinorPlanet.o ${OBJECTDIR}/MinorPlanet_nomain.o;\
	fi

${OBJECTDIR}/Moon_nomain.o: ${OBJECTDIR}/Moon.o Moon.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Moon.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Moon_nomain.o Moon.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Moon.o ${OBJECTDIR}/Moon_nomain.o;\
	fi

${OBJECTDIR}/NaturalObjects_nomain.o: ${OBJECTDIR}/NaturalObjects.o NaturalObjects.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/NaturalObjects.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NaturalObjects_nomain.o NaturalObjects.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/NaturalObjects.o ${OBJECTDIR}/NaturalObjects_nomain.o;\
	fi

${OBJECTDIR}/NewtonLaws_nomain.o: ${OBJECTDIR}/NewtonLaws.o NewtonLaws.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/NewtonLaws.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/NewtonLaws_nomain.o NewtonLaws.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/NewtonLaws.o ${OBJECTDIR}/NewtonLaws_nomain.o;\
	fi

${OBJECTDIR}/Objects_nomain.o: ${OBJECTDIR}/Objects.o Objects.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Objects.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Objects_nomain.o Objects.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Objects.o ${OBJECTDIR}/Objects_nomain.o;\
	fi

${OBJECTDIR}/OpenSolarSystemController_nomain.o: ${OBJECTDIR}/OpenSolarSystemController.o OpenSolarSystemController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/OpenSolarSystemController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OpenSolarSystemController_nomain.o OpenSolarSystemController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/OpenSolarSystemController.o ${OBJECTDIR}/OpenSolarSystemController_nomain.o;\
	fi

${OBJECTDIR}/OpenSolarSystemUI_nomain.o: ${OBJECTDIR}/OpenSolarSystemUI.o OpenSolarSystemUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/OpenSolarSystemUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/OpenSolarSystemUI_nomain.o OpenSolarSystemUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/OpenSolarSystemUI.o ${OBJECTDIR}/OpenSolarSystemUI_nomain.o;\
	fi

${OBJECTDIR}/Planet_nomain.o: ${OBJECTDIR}/Planet.o Planet.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Planet.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Planet_nomain.o Planet.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Planet.o ${OBJECTDIR}/Planet_nomain.o;\
	fi

${OBJECTDIR}/Results_nomain.o: ${OBJECTDIR}/Results.o Results.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Results.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Results_nomain.o Results.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Results.o ${OBJECTDIR}/Results_nomain.o;\
	fi

${OBJECTDIR}/RunSimulationController_nomain.o: ${OBJECTDIR}/RunSimulationController.o RunSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/RunSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RunSimulationController_nomain.o RunSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/RunSimulationController.o ${OBJECTDIR}/RunSimulationController_nomain.o;\
	fi

${OBJECTDIR}/RunSimulationUI_nomain.o: ${OBJECTDIR}/RunSimulationUI.o RunSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/RunSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/RunSimulationUI_nomain.o RunSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/RunSimulationUI.o ${OBJECTDIR}/RunSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/Satellite_nomain.o: ${OBJECTDIR}/Satellite.o Satellite.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Satellite.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Satellite_nomain.o Satellite.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Satellite.o ${OBJECTDIR}/Satellite_nomain.o;\
	fi

${OBJECTDIR}/Simulation_nomain.o: ${OBJECTDIR}/Simulation.o Simulation.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Simulation.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Simulation_nomain.o Simulation.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Simulation.o ${OBJECTDIR}/Simulation_nomain.o;\
	fi

${OBJECTDIR}/SimulationList_nomain.o: ${OBJECTDIR}/SimulationList.o SimulationList.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/SimulationList.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SimulationList_nomain.o SimulationList.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/SimulationList.o ${OBJECTDIR}/SimulationList_nomain.o;\
	fi

${OBJECTDIR}/SolarSystem_nomain.o: ${OBJECTDIR}/SolarSystem.o SolarSystem.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/SolarSystem.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SolarSystem_nomain.o SolarSystem.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/SolarSystem.o ${OBJECTDIR}/SolarSystem_nomain.o;\
	fi

${OBJECTDIR}/SolarSystemList_nomain.o: ${OBJECTDIR}/SolarSystemList.o SolarSystemList.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/SolarSystemList.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SolarSystemList_nomain.o SolarSystemList.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/SolarSystemList.o ${OBJECTDIR}/SolarSystemList_nomain.o;\
	fi

${OBJECTDIR}/SpaceShip_nomain.o: ${OBJECTDIR}/SpaceShip.o SpaceShip.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/SpaceShip.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/SpaceShip_nomain.o SpaceShip.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/SpaceShip.o ${OBJECTDIR}/SpaceShip_nomain.o;\
	fi

${OBJECTDIR}/Star_nomain.o: ${OBJECTDIR}/Star.o Star.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/Star.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Star_nomain.o Star.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/Star.o ${OBJECTDIR}/Star_nomain.o;\
	fi

${OBJECTDIR}/StartSimulationController_nomain.o: ${OBJECTDIR}/StartSimulationController.o StartSimulationController.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/StartSimulationController.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StartSimulationController_nomain.o StartSimulationController.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/StartSimulationController.o ${OBJECTDIR}/StartSimulationController_nomain.o;\
	fi

${OBJECTDIR}/StartSimulationUI_nomain.o: ${OBJECTDIR}/StartSimulationUI.o StartSimulationUI.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/StartSimulationUI.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/StartSimulationUI_nomain.o StartSimulationUI.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/StartSimulationUI.o ${OBJECTDIR}/StartSimulationUI_nomain.o;\
	fi

${OBJECTDIR}/VerletAlgorithm_nomain.o: ${OBJECTDIR}/VerletAlgorithm.o VerletAlgorithm.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/VerletAlgorithm.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/VerletAlgorithm_nomain.o VerletAlgorithm.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/VerletAlgorithm.o ${OBJECTDIR}/VerletAlgorithm_nomain.o;\
	fi

${OBJECTDIR}/main_nomain.o: ${OBJECTDIR}/main.o main.cpp 
	${MKDIR} -p ${OBJECTDIR}
	@NMOUTPUT=`${NM} ${OBJECTDIR}/main.o`; \
	if (echo "$$NMOUTPUT" | ${GREP} '|main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T main$$') || \
	   (echo "$$NMOUTPUT" | ${GREP} 'T _main$$'); \
	then  \
	    ${RM} "$@.d";\
	    $(COMPILE.cc) -g -Dmain=__nomain -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main_nomain.o main.cpp;\
	else  \
	    ${CP} ${OBJECTDIR}/main.o ${OBJECTDIR}/main_nomain.o;\
	fi

# Run Test Targets
.test-conf:
	@if [ "${TEST}" = "" ]; \
	then  \
	    ${TESTDIR}/TestFiles/f1 || true; \
	    ${TESTDIR}/TestFiles/f2 || true; \
	    ${TESTDIR}/TestFiles/f5 || true; \
	    ${TESTDIR}/TestFiles/f4 || true; \
	    ${TESTDIR}/TestFiles/f3 || true; \
	else  \
	    ./${TEST} || true; \
	fi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${TESTDIR}/TestFiles/f2.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
