#include "CopySimulationUI.h"

CopySimulationUI::CopySimulationUI() {
}

CopySimulationUI::~CopySimulationUI() {
}

void CopySimulationUI::run(SimulationList& sl) {
    cout << "\nCOPY SIMULATION" << endl;
    cout << "Presenting the most recent list of simulations..." << endl;
    csc.showList(sl);
    cout << "\nPlease choose one of the previous simulations. \n\nAnswer: ";
    int s;
    cin >> s;
    Simulation* ss = (csc.getSimulation(s,sl));
    cout << endl << *ss;
    cout << endl << "Copying chosen Solar System...\n";
    csc.copySimulation(ss, sl);
    cout << endl << "List updated..." << endl;
    csc.showList(sl);
    cout << endl << "SIMULATION COPIED SUCCESSFULLY" << endl << endl;
}

