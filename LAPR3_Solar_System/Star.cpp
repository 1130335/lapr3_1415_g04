#include "Star.h"

Star::Star() {
    this->name = "unknown";
    this->mass = 0;
    this->equatorial_diameter = 0;
    position[0] = 0;
    position[1] = 0;
    position[2] = 0;
}

Star::Star(string name, double mass, double e_diameter) {
    this->name = name;
    this->mass = mass;
    this->equatorial_diameter = e_diameter;
    position[0] = 0;
    position[1] = 0;
    position[2] = 0;
}

Star::Star(Star& s) {
    this->name = s.getName();
    this->mass = s.getMass();
    this->equatorial_diameter = s.getEquatorialDiameter();
    position[0] = s.position[0];
    position[1] = s.position[1];
    position[2] = s.position[2];
}

Star::~Star() {

}

string Star::getName() {
    return this->name;
}

double Star::getMass() {
    return this->mass;
}

double Star::getEquatorialDiameter() {
    return this->equatorial_diameter;
}

void Star::setName(string name) {
    this->name = name;
}

void Star::setMass(double mass) {
    this->mass = mass;
}

void Star::setEquatorialDiameter(double e_diameter) {
    this->equatorial_diameter = e_diameter;
}

void Star::write(ostream& out) {
    out << ">> Star" << endl;
    out << "Name: " << this->getName() << endl;
    out << "Mass: " << this->getMass() << " Kg" << endl;
    out << "Equatorial Diameter: " << this->getEquatorialDiameter() << " Km" << endl;
    out << endl;
}

ostream& operator<<(ostream &out, Star& s) {
    s.write(out);
    return out;
}
