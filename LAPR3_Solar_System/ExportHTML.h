/* 
 * File:   ExportHTML.h
 * Author: Rui Correia <1130458@isep.ipp.pt>
 *
 * Created on 15 de Janeiro de 2015, 15:56
 */

#ifndef EXPORTHTML_H
#define	EXPORTHTML_H

#include <string>
#include <list>
#include <sstream>
#include <fstream>
#include <string.h>
#include <stdlib.h>
using namespace std;
#include "Simulation.h"
#include "Results.h"

class ExportHTML {
private:
    Simulation* Simul;
    Results r;
    
public:
    ExportHTML();
    ExportHTML(Simulation* exp);
    ~ExportHTML();
    
    void setSimul(Simulation* aux);
    
    void createHTML();
    string Cabecalho();
    string Corpo();
    void InserirDados(ofstream& html);
    string Fim();
    
};

#endif	/* EXPORTHTML_H */

