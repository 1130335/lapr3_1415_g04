#ifndef SOLARSYSTEMLIST_H
#define	SOLARSYSTEMLIST_H

#include <list>
#include <string>
#include <iostream>
#include "SolarSystem.h"

using namespace std;

class SolarSystemList {
public:
    list<SolarSystem*> ssl;
    
    SolarSystemList();
    SolarSystemList(list<SolarSystem*> l);
    ~SolarSystemList();
    
    void add(SolarSystem* ss);
    void remove();
    void showSolarSystems(ostream&);
    
    list<SolarSystem*> getSSL() const;
    void setSSL(list<SolarSystem*> listSS);
    friend ostream & operator<<(ostream &out, SolarSystemList& l);
    
};

#endif	/* SOLARSYSTEMLIST_H */

