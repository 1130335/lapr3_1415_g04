#include "Force.h"

Force::Force(){
    
}

Force::~Force(){
    
}

Force::Force(double fx, double fy, double fz){
    this->fX = fx;
    this->fY = fy;
    this->fZ = fz;
}

Force::Force(Force& f){
    this->fX = f.fX;
    this->fY = f.fY;
    this->fZ = f.fZ;
}

double Force::getFX() const{
    return this->fX;
}

double Force::getFY() const{
    return this->fY;
}

double Force::getFZ() const{
    return this->fZ;
}

void Force::setFX(double fx){
    this->fX=fx;
}

void Force::setFY(double fy){
    this->fY=fy;
}

void Force::setFZ(double fz){
    this->fZ;
}
