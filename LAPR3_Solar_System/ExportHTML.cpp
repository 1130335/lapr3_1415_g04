#include "ExportHTML.h"

ExportHTML::ExportHTML(){
    this->Simul = new Simulation();
}

ExportHTML::~ExportHTML() {
    
}

ExportHTML::ExportHTML(Simulation* aux){
    this->Simul = new Simulation();
    setSimul(aux);
}

void ExportHTML::createHTML() {
    
    stringstream s;
    s << Simul->getName() << ".html";
    string name = s.str();
    char * n = new char[name.length() + 1];
    strcpy(n, name.c_str());
    ofstream html(n);
    html << Cabecalho();
    html << Corpo();
    InserirDados(html);
    html << Fim();
    html.close();
    
}
    /**
     *Devolve o cabeçalho da página HTML
     * @return cabecalho
     */
    string ExportHTML::Cabecalho() {
        string cabecalho;
        cabecalho = "<!DOCTYPE html>\n"
                , "<html>\n"
                , "<head>\n"
                , "<title>Simulation Result</title>\n"
                , "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n"
                , "</head>"
                , "<body>"
                , "<h1>LAPR3_G04</h1>";
        return cabecalho;
    }

    /**
     *Prepara a tabela para as colisões e devolve o início do corpo da página HTML
     * @return corpo
     */
    string ExportHTML::Corpo() {
        string corpo;
        corpo = "<h2>Simulation Result</h2><p>\n" 
		, "<table border = \"1\">\n"
                , "<tr>\n"
                , "<td><b> TimeStep </b></td>\n"
                , "<td><b> Body</b></td>\n"
                , "<td><b> Position X</b></td>\n"
                , "<td><b> Position Y</b></td>\n"
                , "<td><b> Position Z</b></td>\n"
                , "<td><b> Velocity X</b></td>\n"
                , "<td><b> Velocity Y</b></td>\n"
                , "<td><b> Velocity Z</b></td>\n"
                , "</tr>\n";

        return corpo;
    }
 
void ExportHTML::InserirDados(ofstream& html) {
    int i,j, timestep;
    int size = r.getXPos().size();
    timestep = Simul->getTime_Step();
    vector <NaturalObjects*> bodys;
    vector <Objects*> objects;
    
    bodys = Simul->getVNObj();
    objects = Simul->getVObj();
    
    vector<double> xVel;
    vector<double> yVel;
    vector<double> zVel;
    vector<double> xPos;
    vector<double> yPos;
    vector<double> zPos;
    
    xVel = r.getXVel();
    yVel = r.getYVel();
    zVel = r.getZVel();
    xPos = r.getXPos();
    yPos = r.getYPos();
    zPos = r.getZPos();
    
    string novosDados;
    for(i=0; i < Simul->getEnd_Time(); i= (i + timestep)){
        for(j=0; j<size; j++){
            novosDados = "<tr>"
            , "<td>" , i , "</td>\n"
            , "<td>" , bodys[j] , "</td>\n"
            , "<td>" , xPos[i] , "</td>\n"
            , "<td>" , yPos[i] , "</td>\n"
            , "<td>" , zPos[i] , "</td>\n"
            , "<td>" , xVel[i] , "</td>\n"
            , "<td>" , yVel[i] , "</td>\n"
            , "<td>" , zVel[i] , "</td>\n"
            , "</tr>\n";

            html << novosDados;
        }
    }
}

/**
 *Devolve o fim da página HTML
 * @return fim
 */
string ExportHTML::Fim() {
    string fim;
    fim = "</table>\n"
                    , "</body>\n"
                    , "</html>\n";
    return fim;
}

    
void ExportHTML::setSimul(Simulation* aux){
    Simul = aux;
}
