#ifndef BODY_H
#define	BODY_H

#include <string>
#include <vector>
#include <iostream>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Body : public NaturalObjects {
public:
    vector<Moon*> moons;
    Body();
    Body(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    ~Body();
    Body(Body&);
    void write(ostream&);

    void addMoons(Moon*);
    void showMoons();

};


#endif

