#include "StartSimulationUI.h"

StartSimulationUI::StartSimulationUI() {

}

StartSimulationUI::~StartSimulationUI() {

}

void StartSimulationUI::showListSolarSystems(SolarSystemList& lss) {
    cout << lss << endl;
}

void StartSimulationUI::chooseSolarSystem(int s, SolarSystemList& ssl) {
    int cont = 1;
    SolarSystem* ss;
    list<SolarSystem*> aux = ssl.getSSL();

    while (cont != s) {
        aux.pop_front();
        cont++;
    }

    ss = aux.front();

    ssc.setSolarSystem(ss);
}

void StartSimulationUI::run(SolarSystemList& ssl, SimulationList& sl) {
    cout << "\nCREATE NEW SIMULATION" << endl;
    cout << "Choose a solar system from the following:" << endl;
    showListSolarSystems(ssl);
    cout << "\nAnswer: ";
    int s = 0;
    cin >> s;
    while (s < 0 || s > ssl.getSSL().size()) {
        cout << "\nYour answer was invalid... \n\n Answer:";
        cin >> s;
    }
    chooseSolarSystem(s, ssl);

    double aux;
    string aux1;
    cout << "\nInformation about the Simulation...\n";
    cout << "Name: ";
    cin >> aux1;
    ssc.setName(aux1);
    cout << "Start Time: ";
    cin >> aux;
    ssc.setStart_time(aux);
    cout << "End Time: ";
    cin >> aux;
    ssc.setEnd_time(aux);
    cout << "Time Step: ";
    cin >> aux;
    ssc.setTime_Step(aux);

    cout << "\nDo you want to choose bodies for your simulation? \n1 - yes \n0 - no" << endl << endl;
    int bodies;
    cout << "Answer: ";
    cin >> bodies;
    while (bodies < 0 || bodies > 1) {
        cout << "Your answer was invalid. Enter 1 for yes or 0 for no.\n\nAnswer: ";
        cin >> bodies;
    }

    int nBody = 0;
    while (bodies == 1) {
        cout << "\nList of Natural Objects for the chosen Solar System..." << endl;
        ssc.showListNO();
        cout << "\nEnter the number of the natural object you want to add in the simulation. \n\nAnswer: ";
        cin >> nBody;
        ssc.addNO(nBody);

		cout << "\nBody Addictional Information:\n";
		double px, py, pz;
		cout << "Position x: ";
		cin >> px;
		cout << "Position y: ";
		cin >> py;
		cout << "Position z: ";
		cin >> pz;
		ssc.setPosition(px, py, pz);

		double vx, vy, vz;
		cout << "\nVelocity x: ";
		cin >> vx;
		cout << "Velocity y: ";
		cin >> vy;
		cout << "Velocity z: ";
		cin >> vz;
		ssc.setVelocity(vx, vy, vz);

        cout << "\nDo you wish to add more bodies? \n1 - yes \n0 - no \n\nAnswer: ";
        cin >> bodies;
        while (bodies < 0 || bodies > 1) {
            cout << "Your answer was invalid. Enter 1 for yes or 0 for no.\n\nAnswer: ";
            cin >> bodies;
        }
    }

    int objs = 0;
    cout << "\nDo you want to choose objects for your simulation? \n1 - yes \n0 - no \n\nAnswer: ";
    cin >> objs;
    while (objs < 0 || objs > 1) {
        cout << "Your answer was invalid. Enter 1 for yes or 0 for no.\n\nAnswer: ";
        cin >> objs;
    }


    int nObj = 0;
    while (objs == 1) {
        cout << "List of Objects for the chosen Solar System..." << endl;
        ssc.showListO();
        cout << "\nEnter the number of the object you want to add in the simulation. \n\nAnswer: ";
        cin >> nObj;
        ssc.addO(nObj);
        cout << "\nDo you wish to add more objects? \n1 - yes \n0 - no \n\nAnswer: ";
        cin >> objs;
        while (objs < 0 || objs > 1) {
            cout << "Your answer was invalid. Enter 1 for yes or 0 for no.\n\nAnswer: ";
            cin >> objs;
        }
    }
    
    Simulation* s1 = &(ssc.getSimulation());
    sl.add(s1);
    cout << "\n\nSIMULATION SUCCESSFULLY CREATED\n" << endl;
}

