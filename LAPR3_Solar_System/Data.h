/* 
 * File:   Data.h
 * Author: Eduardo Pinto <1130466@isep.ipp.pt>
 *
 * Created on 16 de Janeiro de 2015, 21:26
 */

#ifndef DATA_H
#define	DATA_H

#include <iostream>
#include <time.h>

using namespace std;

class Data
{
private:
	int ano;			// qualquer ano
	int mes;			// 1-12
	int dia;			// 1-31 dependente do m�s

	static int diasPorMes[];
	int validaDia(int d) const;      // Confirma o valor do dia baseado no mes e ano 
	int validaMes(int m) const;
	int validaAno(int a) const;
	bool anoBissexto(int a) const;   // Testa se ano � bissexto

public:

	Data();
	Data(int a, int m, int d);
	Data(const Data &d);
	~Data();

	void setAno(int a);
	void setMes(int m);
	void setDia(int d);
	void setData(Data dt);

	int getAno() const;
	int getMes() const;
	int getDia() const;
	int getAnos() const;

	void listar() const;
};

#endif	/* DATA_H */

