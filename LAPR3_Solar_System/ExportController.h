/* 
 * File:   ExportCSV.h
 * Author: Jony
 *
 * Created on 14 de Janeiro de 2015, 23:02
 */

#ifndef EXPORTCONTROLLER_H
#define	EXPORTCONTROLLER_H

#include <string>
#include <list>
#include <sstream>
#include <fstream>
#include <string.h>
#include <stdlib.h>

using namespace std;
#include "Simulation.h"
#include "NaturalObjects.h"
#include "SolarSystem.h"
#include "SimulationList.h"
#include "Results.h"

class ExportController {
private:
    
    Simulation* s;
    Results r;

public:
    ExportController();
    ~ExportController();
    
    void exportCSV(Simulation* si);
    void verificaList();

};
#endif	/* EXPORTCONTROLLER_H */

