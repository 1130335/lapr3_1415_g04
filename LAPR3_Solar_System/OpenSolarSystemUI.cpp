#include "OpenSolarSystemUI.h"

OpenSolarSystemUI::OpenSolarSystemUI(){
}

OpenSolarSystemUI::~OpenSolarSystemUI(){
}

/**
 * Metodo para mostrar a lista de todos os sistemas solares
 * @param lss
 */
void OpenSolarSystemUI::showList(SolarSystemList &solar){
    setMaxSS(solar.getSSL().size());
    cout << solar << endl;
}

/**
 *  Metodod que escreve a informação relativa ao sistema solar escolhido pelo utilizador
 * recebendo por parametro o "index" ao qual o Sistema Solar escolhido esta na lista
 * @param index -> subtrair 1 para obter a possição certa na lista.
 */
void OpenSolarSystemUI::showInfoSS(int index){
    index = (index - 1);
    setSS(abrirc.getInfoSS(index));
    cout << *SS << endl;
}

void OpenSolarSystemUI::menu(SolarSystemList &solar){
    int opcao = -1;
    int SSchosen = -1;
    abrirc.setLSS(solar.getSSL());
    do{
        cout << "What action you want to perform? \n\t1 - List all Solar Systems \n\t0 - Exit\n" << endl;
        cin >> opcao;
            if(opcao == 1){

                do {
                    cout << "List of all the Solar System! " << endl;
                    
                    showList(solar);
                    cout << "What is the solar system you want to open? \n\t0 - Exit " << endl;
                    cin >> SSchosen;
                        if(SSchosen == 0){
                            cout << "Exit the opening of the solar system!\n "<< endl;
                        }else if(SSchosen > maxSS){
                            cout << "The value entered does not match any valid Solar System, please enter a valid value!\n " << endl;
                        }else{
                            showInfoSS(SSchosen);
                            cout << "\n" << endl;
                        } 
                } while (SSchosen != 0);
            }else if (opcao == 0) {
                cout << "Exit the opening of the solar system successfully! \n" << endl;
            } else {
                cout << "Enter one of the intended numbers!" << endl;
            }
    } while (opcao != 0);
}

SolarSystem* OpenSolarSystemUI::getSS(){
    return SS;
}

void OpenSolarSystemUI::setSS(SolarSystem* solar){
    SS = solar;
}

void OpenSolarSystemUI::setMaxSS(int num){
	maxSS = num;
}

int OpenSolarSystemUI::getMaxSS(){
	return maxSS;
}
