#ifndef METEOROID_H
#define	METEOROID_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Meteoroid : public NaturalObjects {
public:
    vector<Moon*> moons;
    Meteoroid();
    Meteoroid(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    Meteoroid(Meteoroid&);
    ~Meteoroid();
    void write(ostream&);
    
    void addMoons(Moon*);
    void showMoons();
};

#endif	/* METEOROID_H */

