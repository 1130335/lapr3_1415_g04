#include "CreateSSController_FT.h"


CreateSSController_FT::CreateSSController_FT() {
    ss1 = new SolarSystem();
}

CreateSSController_FT::~CreateSSController_FT() {
    
}

void CreateSSController_FT::run() {
    cout << "TESTING CREATESOLARSYSTEMCONTROLLER" << endl;
    cout << "Setting expected solar system..." << endl;
    
    SolarSystemList sslTest;
    Planet* p = new Planet();
    Satellite* s = new Satellite();
    Star st;
    ss1->setName("Solar_System_1");
    ss1->setStar(st);
    ss1->addToNaturalObj(p);
    ss1->addToObjects(s);
    
    cout << "Setting the result of this test..." << endl;
    
    cssc.createStar("Sun", 0, 0);
    cssc.createPlanet("Name", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "Star");
    cssc.createSatellite(0, 0, 0, 0, 0);
    cssc.addSSToList(sslTest);
    
    if((sslTest.getSSL()).back()->getName() == ss1->getName()) {
        cout << "This test was successful!!!" << endl;
    }
    
    cout << endl << endl;
    
}
