#include "Simulation.h"

Simulation::Simulation() {
    setName("Simulation");
    setStart_Time(0);
    setEnd_Time(0);
    setTime_Step(0);
}

Simulation::Simulation(SolarSystem* s, string n, double st, double et, double ts, int rt) {
    this->solar = s;
    this->name = n;
    this->start_time = st;
    this->end_time = et;
    this->time_step = ts;
    this->reference_time = rt;
}

Simulation::Simulation(Simulation& s) {
    this->solar = s.getSolar();
    this->name = s.getName();
    this->start_time = s.getStart_Time();
    this->end_time = s.getEnd_Time();
    this->time_step = s.getTime_Step();
    this->reference_time = s.getReference_time();
}

Simulation::~Simulation() {
}

void Simulation::setSolarSystem(SolarSystem* s) {
    solar = s;
    s_star = solar->getStar();
}

double Simulation::getMass(NaturalObjects* no) {
    return no->getMass();
}

vector<double> Simulation::getPosition(NaturalObjects* no) {
    return no->getPosition();
}

vector<double> Simulation::getVelocity(NaturalObjects* no) {
    return no->getVelocity();
}

void Simulation::setName(string n) {
    name = n;
}

void Simulation::setStart_Time(double st) {
    start_time = st;
}

void Simulation::setEnd_Time(double et) {
    end_time = et;
}

void Simulation::setTime_Step(double ts) {
    time_step = ts;
}

void Simulation::setReference_Time(int rt){
    reference_time = rt;
}

void Simulation::setDelta_Time(double dt){
    delta_time=dt;
}

SolarSystem* Simulation::getSolar() const {
    return solar;
}

string Simulation::getName() const {
    return name;
}

double Simulation::getStart_Time() {
    return start_time;
}

double Simulation::getEnd_Time() {
    return end_time;
}

double Simulation::getTime_Step() {
    return time_step;
}

int Simulation::getReference_time() {
    return reference_time;
}

double Simulation::getDelta_Time(){
    return delta_time;
}

vector<NaturalObjects*> Simulation::getVNObj() {
    return this->bodys;
}

vector<Objects*> Simulation::getVObj() {
    return this->objects;
}

void Simulation::setVNObj(vector<NaturalObjects*> vno) {
    this->bodys = vno;
}

void Simulation::setVObj(vector<Objects*> vo) {
    this->objects = vo;
}

void Simulation::write(ostream& out) const {
    out << "Simulation: " << endl;
}

ostream & operator<<(ostream &out, Simulation& s) {
    s.write(out);
    return out;
}

void Simulation::addBodys(NaturalObjects* no) {
    bodys.push_back(no);
}

void Simulation::addObjects(Objects* o) {
    objects.push_back(o);
}

