#ifndef SATELLITE_H
#define	SATELLITE_H

#include "Objects.h"
#include <string>

using namespace std;

class Satellite: public Objects{
    
private:
    
public:
    Satellite();
    Satellite(double v, double m, double g, double o, int id);
    ~Satellite();
    Satellite(const Satellite& s);
    void write(ostream& out) const;
};

#endif	/* SATELLITE_H */
