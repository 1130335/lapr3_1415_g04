#include "RunSimulationController.h"

RunSimulationController::RunSimulationController() {

    setName("Não Definido");
    setForce(0);
    setM1(0);
    setM2(0);
    this->G = (pow(6.674, -11));
    setDistance(0);
    setUnitVector(0);
    setVelocity(0);

}

RunSimulationController::RunSimulationController(string n, double f, double gc, double massa1, double massa2, double d, double uv, double v, SolarSystem& s) {

    this->name = n;
    this->force = f;
    this->G = gc;
    this->m1 = massa1;
    this->m2 = massa2;
    this->distance = d;
    this->unitVector = uv;
    this->velocity = v;
    this->solar = s;

}

RunSimulationController::RunSimulationController(RunSimulationController& s) {

    this->name = s.getName();
    this->force = s.getForce();
    this->G = s.getGravitacionalConstant();
    this->m1 = s.getM1();
    this->m2 = s.getM2();
    this->distance = s.getDistance();
    this->unitVector = s.getUnitVector();
    this->velocity = s.getVelocity();
    this->solar = s.getSolar();

}

RunSimulationController::~RunSimulationController() {
}

void RunSimulationController::setName(string n) {
    name = n;
}

void RunSimulationController::setForce(double f) {
    force = f;
}

void RunSimulationController::setM1(double massa1) {
    m1 = massa1;
}

void RunSimulationController::setM2(double massa2) {
    m2 = massa2;
}

void RunSimulationController::setDistance(double d) {
    distance = d;
}

void RunSimulationController::setUnitVector(double uv) {
    unitVector = uv;
}

void RunSimulationController::setVelocity(double v) {
    velocity = v;
}

string RunSimulationController::getName() const {
    return name;
}

double RunSimulationController::getForce() const {
    return force;
}

double RunSimulationController::getGravitacionalConstant() const {
    return G;
}

double RunSimulationController::getM1() const {
    return m1;
}

double RunSimulationController::getM2() const {
    return m2;
}

double RunSimulationController::getDistance() const {
    return distance;
}

double RunSimulationController::getUnitVector() const {
    return unitVector;
}

double RunSimulationController::getVelocity() const {
    return velocity;
}

SolarSystem RunSimulationController::getSolar() const {
    return solar;
}

//vector<double> RunSimulationController::getMass(vector <NaturalObjects*> natObj) {
//    vector<double> mass;
//    for (int i = 0; i < natObj.size(); i++) {
//        mass[i] = natObj[i]->getMass();
//    }
//    return mass;
//}

void RunSimulationController::runVerlet(Simulation* s) {

    VerletAlgorithm va;
//    vector<double> mass = getMass(s->getVNObj());
//    vector<double> position = s->getPosition(no);
//    vector<double> velocity = s->getVelocity(no);
    va.setSimulation(s);
    va.Integrate();
    
    /**/
            //    bool RunSimulationController::runSimulation() { --
            //    VerletAlgorithm va = VerletAlgorithm();--
            //    vector<double> mass = simu->getBodyMass(); --
            //    vector<Position> position = simu->getBodyPosition();
            //    vector<Velocity> velocity = simu->getBodyVelocity();
            //    va.verletAlgorithm(simu, mass, velocity, position);
            //
            //}

}


void RunSimulationController::showList(SimulationList& sl) {
    cout << sl;
}

Simulation* RunSimulationController::chooseSimulation(int op, SimulationList& sl) {

    int cont = 1;
    Simulation* s;
    list<Simulation*> aux = sl.getSimulationList();

    while (cont != op) {
        aux.pop_front();
        cont++;
    }

    s = aux.front();

    return s;
}