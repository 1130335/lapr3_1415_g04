#include "MinorPlanet.h"

MinorPlanet::MinorPlanet() : NaturalObjects() {

}

MinorPlanet::MinorPlanet(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) : NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz, o) {

}

MinorPlanet::MinorPlanet(MinorPlanet& mp) : NaturalObjects(mp) {

}

MinorPlanet::~MinorPlanet() {

}

void MinorPlanet::write(ostream& out) {
    out << "Body Type: Minor Planet" << endl;
    NaturalObjects::write(out);
    out << endl;
}

void MinorPlanet::addMoons(Moon* m) {
    this->moons.push_back(m);
}

void MinorPlanet::showMoons() {
    for (int i = 0; i < this->moons.size(); i++) {
        cout << *this->moons[i];
    }
}

