#ifndef COPYSIMULATIONUI_H
#define	COPYSIMULATIONUI_H

#include "CopySimulationController.h"
#include "SimulationList.h"
#include <iostream>

using namespace std;

class CopySimulationUI {
public:
    CopySimulationUI();
    virtual ~CopySimulationUI();
    void run(SimulationList&);
private:
    CopySimulationController csc;
};

#endif	/* COPYSIMULATIONUI_H */

