#ifndef STARTSIMULATIONUI_H
#define	STARTSIMULATIONUI_H

#include <iostream>
#include <list>

#include "StartSimulationController.h"
#include "SolarSystemList.h"
#include "SimulationList.h"

using namespace std;

class StartSimulationUI {
private:

    StartSimulationController ssc;

public:
    StartSimulationUI();
    ~StartSimulationUI();

    void showListSolarSystems(SolarSystemList& ssl);
    void chooseSolarSystem(int, SolarSystemList&);

    void run(SolarSystemList& ssl, SimulationList& sl);

};

#endif	/* STARTSIMULATIONUI_H */

