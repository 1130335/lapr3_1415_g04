#include "ImportSolarSystemUI.h"

ImportSolarSystemUI::ImportSolarSystemUI() {

}

ImportSolarSystemUI::~ImportSolarSystemUI() {

}

void ImportSolarSystemUI::run(SolarSystemList& ssl) {
    string nCSV;
    cout << "\nIMPORT SOLAR SYSTEM FROM CSV FILE" << endl;
    cout << "Creating Solar System's Star..." << endl;
    string name;
    float mass, diameter;
    cout << "Name: ";
    cin >> name;
    cout << "Mass: ";
    cin >> mass;
    cout << "Equatorial Diameter: ";
    cin >> diameter;
    cout << endl;

    cout << "Indicate the name of the file you want to use. This file must be in the project folder. \nFile name: ";
    cin >> nCSV;
    issc.importSolarSystemCSV(nCSV, ssl);
    cout << "This is the solar system that was imported...\n";
    issc.showInfo(name, mass, diameter);
    issc.addToSSL(ssl);
    cout << "SOLAR SYSTEM IMPORTED SUCCESSFULLY\n" << endl;

}
