#include "Comet.h"

Comet::Comet() : NaturalObjects() {
    this->ellipticalOrbit = 0;
}

Comet::Comet(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, double eo, string o) : NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz, o) {
    this->ellipticalOrbit = eo;
}

Comet::Comet(Comet& c) : NaturalObjects(c) {
    this->ellipticalOrbit = c.getEllipticalOrbit();
}

Comet::~Comet() {

}

double Comet::getEllipticalOrbit() const {
    return this->ellipticalOrbit;
}

void Comet::setEllipticalOrbit(double eo) {
    this->ellipticalOrbit = eo;
}

void Comet::write(ostream& out) {
    out << "Body Type: Comet" << endl;
    out << "Elliptical Orbit: " << this->getEllipticalOrbit() << endl;
    NaturalObjects::write(out);
    out << endl;
}

void Comet::addMoons(Moon* m) {
    this->moons.push_back(m);
}

void Comet::showMoons() {
    for (int i = 0; i < this->moons.size(); i++) {
        cout << *this->moons[i];
    }
}

