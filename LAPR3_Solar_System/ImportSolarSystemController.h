#ifndef IMPORTSOLARSYSTEMCONTROLLER_H
#define	IMPORTSOLARSYSTEMCONTROLLER_H

#include "CSV.h"
#include "SolarSystem.h"
#include "SolarSystemList.h"
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

class ImportSolarSystemController {
private:
    SolarSystem* ss;
    CSV file_csv;
public:
    ImportSolarSystemController();
    ~ImportSolarSystemController();

    void createStar(string, double, double);
    void importSolarSystemCSV(string, SolarSystemList&);
    void showInfo(string, double, double);
    void addToSSL(SolarSystemList&);
};

#endif	/* IMPORTSOLARSYSTEMCONTROLLER_H */

