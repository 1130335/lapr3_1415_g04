#ifndef COPYSOLARSYSTEMCONTROLLER_H
#define	COPYSOLARSYSTEMCONTROLLER_H

#include "SolarSystem.h"
#include "SolarSystemList.h"
#include <list>
#include <sstream>
#include <iostream>

using namespace std;

class CopySolarSystemController {
public:
    CopySolarSystemController();
    ~CopySolarSystemController();
    
    void showList(SolarSystemList&);
    SolarSystem* getSolarSystem(int, SolarSystemList&);
    void copySolarSystem(SolarSystem*, SolarSystemList&);
};

#endif	/* COPYSOLARSYSTEMCONTROLLER_H */

