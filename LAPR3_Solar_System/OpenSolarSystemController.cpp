#include "OpenSolarSystemController.h"

OpenSolarSystemController::OpenSolarSystemController() {
}

OpenSolarSystemController::~OpenSolarSystemController() {
}

/**
 * Metodo que atualiza a lss.
 */
int OpenSolarSystemController::alterListaSistemasSolares() {
    SolarSystemList listaSS;
    setLSS(listaSS.getSSL());
    return (lss.size());
}

/**
 *   Metodod que escreve a informação relativa ao sistema solar escolhido pelo utilizador
 * recebendo por parametro o "index" ao qual o Sistema Solar escolhido esta na lista
 * @param index -> o index ja esta direito
 */
SolarSystem* OpenSolarSystemController::getInfoSS(int index) {
    list<SolarSystem*>::iterator i;
    int count = 0;
    for (i = lss.begin(); i != lss.end(); ++i) {
        if (count == index) {
            //cout << *i << endl;
            return *i;
        }
        count++;
    }
}

/**
 *      ### Secção para os get's e set's ###
 */

list<SolarSystem*> OpenSolarSystemController::getLSS() const {
    return lss;
}

void OpenSolarSystemController::setLSS(list<SolarSystem*> listaSS) {
    lss = listaSS;
}

