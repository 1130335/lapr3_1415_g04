#include "EditSolarSystemUI.h"

EditSolarSystemController::EditSolarSystemController(){
}

EditSolarSystemController::~EditSolarSystemController(){
}

void EditSolarSystemController::setChosen(SolarSystem* ss){
    chosen = ss;
}

/**
 * Metodo que atualiza a lss.
 */
int EditSolarSystemController::alterListaSistemasSolares(){
    SolarSystemList listaSS;
    setLSS(listaSS.getSSL());
    return (lss.size());
}

SolarSystem* EditSolarSystemController::getInfoSS(int index){
    list<SolarSystem*>::iterator i;
    int count=0;
        for (i = lss.begin(); i != lss.end(); ++i) {
            if (count == index){
                setChosen(*i);
                return *i;
            }
            count++;
        }
}

/**
 *      ### Secção para os get's e set's ###
 */

list<SolarSystem*> EditSolarSystemController::getLSS() const{
    return lss;
}

void EditSolarSystemController::setLSS(list<SolarSystem*> listaSS){
    lss = listaSS;
}

/**
 * vai alterar a lista de corpos naturais
 * @return  retorna o index final dos corpos naturais
 */
int EditSolarSystemController::alterNaturalBody(){
    setNB(chosen->getNObj());
    return (nObj.size());
}

void EditSolarSystemController::setNB(list<NaturalObjects*> listaNB){
    nObj = listaNB;
}

list<NaturalObjects*> EditSolarSystemController::getNB() const{
    return nObj;
}

void EditSolarSystemController::addNewNaturalBody(NaturalObjects* newNO){
    chosen->addToNaturalObj(newNO);
}

void EditSolarSystemController::deleteNB(int index){
    index = (index-1);
    list<NaturalObjects*>::iterator i;
    list<NaturalObjects*> aux;
    int count =0;
        for (i = nObj.begin(); i != nObj.end(); ++i) {
            if (count != index){
                aux.push_back(nObj.front());
				nObj.pop_front();
            }
            count++;
        }
    nObj.swap(aux);
    chosen->setNObj(nObj);
}


/**
 *      MAN MADE OBJECT
 */

/**
 * vai alterar a lista de corpos naturais
 * @return  retorna o index final dos corpos naturais
 */
int EditSolarSystemController::alterManMadeObject(){
    setMMO(chosen->getObj());
    return (obj.size());
}

void EditSolarSystemController::addNewManMadeObject(Objects* Newobj){
    chosen->addToObjects(Newobj);
}

void EditSolarSystemController::deleteMMO(int index){
    index = (index-1);
    list<Objects*>::iterator i;
    list<Objects*> aux;
    int count =0;
        for (i = obj.begin(); i != obj.end(); ++i) {
            if (count != index){
                aux.push_back(obj.front());
				obj.pop_front();
            }
            count++;
        }
    obj.swap(aux);
    chosen->setObj(obj);
}

void EditSolarSystemController::setMMO(list<Objects*> listaMMO){
    obj = listaMMO;
}

list<Objects*> EditSolarSystemController::getMMO() const{
    return obj;
}
