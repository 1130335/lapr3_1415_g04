#include "NaturalObjects.h"

NaturalObjects::NaturalObjects() {
    this->name = "unknown";
    this->Px = 0;
    this->Py = 0;
    this->Pz = 0;
    this->Vx = 0;
    this->Vy = 0;
    this->Vz = 0;
    this->period_revolution = 0;
    this->orbital_speed = 0;
    this->inclination_axis_orbit = 0;
    this->semimajor_axis = 0;
    this->eccentricity = 0;
    this->orbit_inclination = 0;
    this->equatorial_diameter = 0;
    this->mass = 0;
    this->density = 0;
    this->escape_velocity = 0;
    this->perihelion = 0;
    this->aphelion = 0;
    this->orbiting = "Star";
    this->mean_anomaly = 0;
    this->long_perihelion = 0;
    this->long_ascending = 0;
}

NaturalObjects::NaturalObjects(string n, double pr, double os, double iao, double ed, double m, double d, double ev, double sa, double e, double oi, double p, double a, double px, double py, double pz, double vx, double vy, double vz, string o) {
    this->name = n;
    this->Px = px;
    this->Py = py;
    this->Pz = pz;
    this->Vx = vx;
    this->Vy = vy;
    this->Vz = vz;
    this->period_revolution = pr;
    this->orbital_speed = os;
    this->inclination_axis_orbit = iao;
    this->semimajor_axis = sa;
    this->eccentricity = e;
    this->orbit_inclination = oi;
    this->equatorial_diameter = ed;
    this->mass = m;
    this->density = d;
    this->escape_velocity = ev;
    this->perihelion = p;
    this->aphelion = a;
    this->orbiting = o;
    this->mean_anomaly = 0;
    this->long_perihelion = 0;
    this->long_ascending = 0;
}

NaturalObjects::NaturalObjects(NaturalObjects& no) {
    this->name = no.getName();
    this->Px = no.getPx();
    this->Py = no.getPy();
    this->Pz = no.getPz();
    this->Vx = no.getVx();
    this->Vy = no.getVy();
    this->Vz = no.getVz();
    this->period_revolution = no.getPeriodRevolution();
    this->orbital_speed = no.getOrbitalSpeed();
    this->inclination_axis_orbit = no.getInclinationAxisOrbit();
    this->semimajor_axis = no.getSemiMajorAxis();
    this->eccentricity = no.getEccentricity();
    this->orbit_inclination = no.getOrbitInclination();
    this->equatorial_diameter = no.getEquatorialDiameter();
    this->mass = no.getMass();
    this->density = no.getDensity();
    this->escape_velocity = no.getEscapeVelocity();
    this->perihelion = no.getPerihelion();
    this->aphelion = no.getAphelion();
    this->orbiting = no.getOrbiting();
    this->mean_anomaly = no.getMeanAnomaly();
    this->long_perihelion = no.getLongitudeP();
    this->long_ascending = no.getLongitudeAsc();
}

NaturalObjects::~NaturalObjects() {

}

string NaturalObjects::getReference() {
    return this->reference;
}

void NaturalObjects::setReference(string r) {
    this->reference = r;
}

string NaturalObjects::getName() {
    return this->name;
}

double NaturalObjects::getPx() {
    return this->Px;
}

double NaturalObjects::getPy() {
    return this->Py;
}

double NaturalObjects::getPz() {
    return this->Pz;
}

double NaturalObjects::getVx() {
    return this->Vx;
}

double NaturalObjects::getVy() {
    return this->Vy;
}

double NaturalObjects::getVz() {
    return this->Vz;
}

double NaturalObjects::getPeriodRevolution() {
    return this->period_revolution;
}

double NaturalObjects::getOrbitalSpeed() {
    return this->orbital_speed;
}

double NaturalObjects::getInclinationAxisOrbit() {
    return this->inclination_axis_orbit;
}

double NaturalObjects::getSemiMajorAxis() {
    return this->semimajor_axis;
}

double NaturalObjects::getEccentricity() {
    return this->eccentricity;
}

double NaturalObjects::getOrbitInclination() {
    return this->orbit_inclination;
}

double NaturalObjects::getEquatorialDiameter() {
    return this->equatorial_diameter;
}

double NaturalObjects::getMass() {
    return this->mass;
}

double NaturalObjects::getDensity() {
    return this->density;
}

double NaturalObjects::getEscapeVelocity() {
    return this->escape_velocity;
}

double NaturalObjects::getPerihelion() {
    return this->perihelion;
}

double NaturalObjects::getAphelion() {
    return this->aphelion;
}

string NaturalObjects::getOrbiting() {
    return this->orbiting;
}

double NaturalObjects::getMeanAnomaly() {
    return this->mean_anomaly;
}

double NaturalObjects::getLongitudeAsc() {
    return this->long_ascending;
}

double NaturalObjects::getLongitudeP() {
    return this->long_perihelion;
}

void NaturalObjects::setName(string n) {
    this->name = n;
}

void NaturalObjects::setPx(double px) {
    this->Px = px;
}

void NaturalObjects::setPy(double py) {
    this->Py = py;
}

void NaturalObjects::setPz(double pz) {
    this->Pz = pz;
}

void NaturalObjects::setVx(double vx) {
    this->Vx = vx;
}

void NaturalObjects::setVy(double vy) {
    this->Vy = vy;
}

void NaturalObjects::setVz(double vz) {
    this->Vz = vz;
}

void NaturalObjects::setPosition(double px, double py, double pz) {
    position[0] = px;
    position[1] = py;
    position[2] = pz;
}

void NaturalObjects::setVelocity(double vx, double vy, double vz) {
    velocity[0] = vx;
    velocity[1] = vy;
    velocity[2] = vz;
}

vector<double> NaturalObjects::getPosition() {
    return position;
}

vector<double> NaturalObjects::getVelocity() {
    return velocity;
}

void NaturalObjects::setPeriodRevolution(double pr) {
    this->period_revolution = pr;
}

void NaturalObjects::setOrbitalSpeed(double os) {
    this->orbital_speed = os;
}

void NaturalObjects::setInclinationAxisOrbit(double iao) {
    this->inclination_axis_orbit = iao;
}

void NaturalObjects::setSemiMajorAxis(double sa) {
    this->semimajor_axis = sa;
}

void NaturalObjects::setEccentricity(double e) {
    this->eccentricity = e;
}

void NaturalObjects::setOrbitInclination(double oi) {
    this->orbit_inclination = oi;
}

void NaturalObjects::setEquatorialDiameter(double ed) {
    this->equatorial_diameter = ed;
}

void NaturalObjects::setMass(double m) {
    this->mass = m;
}

void NaturalObjects::setDensity(double d) {
    this->density = d;
}

void NaturalObjects::setEscapeVelocity(double ev) {
    this->escape_velocity = ev;
}

void NaturalObjects::setPerihelion(double p) {
    this->perihelion = p;
}

void NaturalObjects::setAphelion(double a) {
    this->aphelion = a;
}

void NaturalObjects::setOrbiting(string o) {
    this->orbiting = o;
}

void NaturalObjects::setLongitudeAsc(double la) {
    this->long_ascending = la;
}

void NaturalObjects::setLongitudeP(double lp) {
    this->long_perihelion = lp;
}

void NaturalObjects::setMeanAnomaly(double ma) {
    this->mean_anomaly = ma;
}

void NaturalObjects::write(ostream& out) {
    out << "Name: " << this->getName() << endl;
    out << "Px: " << this->getPx() << " AU" << endl;
    out << "Py: " << this->getPy() << " AU" << endl;
    out << "Pz: " << this->getPz() << " AU" << endl;
    out << "Vx: " << this->getVx() << " m/s" << endl;
    out << "Vy: " << this->getVy() << " m/s" << endl;
    out << "Vz: " << this->getVz() << " m/s" << endl;
    out << "Mass: " << this->getMass() << endl;
    out << "Density: " << this->getDensity() << endl;
    out << "Eccentricity: " << this->getEccentricity() << endl;
    out << "Equatorial Diameter: " << this->getEquatorialDiameter() << " km" << endl;
    out << "Inclination of Axis to Orbit: " << this->getInclinationAxisOrbit() << endl;
    out << "Orbital speed: " << this->getOrbitalSpeed() << " m/s" << endl;
    out << "Period of revolution: " << this->getPeriodRevolution() << " years" << endl;
    out << "Escape Velocity: " << this->getEscapeVelocity() << " km/s" << endl;
    out << "Semimajor Axis: " << this->getSemiMajorAxis() << " km" << endl;
    out << "Orbit Inclination: " << this->getOrbitInclination() << " degree" << endl;
    out << "Perihelion: " << this->getPerihelion() << " km" << endl;
    out << "Aphelion: " << this->getAphelion() << " km" << endl;
    out << "Orbiting: " << this->getOrbiting() << endl;
    out << "Mean Anomaly: " << this->getMeanAnomaly() << " rad" << endl;
    out << "Longitude of perihelion: " << this->getLongitudeP() << " º" << endl;
    out << "Longitude of ascending node: " << this->getLongitudeAsc() << " º" << endl;
}

ostream & operator<<(ostream &out, NaturalObjects& no) {
    no.write(out);
    return out;
}
