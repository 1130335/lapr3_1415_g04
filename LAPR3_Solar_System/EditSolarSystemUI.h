/* 
 * File:   EditSolarSystem.h
 * Author: Rui Correia <1130458@isep.ipp.pt>
 *
 * Created on 9 de Janeiro de 2015, 15:06
 */

#ifndef EDITSOLARSYSTEM_H
#define	EDITSOLARSYSTEM_H

#include "EditSolarSystemController.h"
#include "SolarSystemList.h"
#include "SolarSystem.h"
#include <iostream>
#include <list>

using namespace std;
class EditSolarSystemUI {
private:
    EditSolarSystemController editc;
    int maxSS;
    int maxNB;
    int maxMMO;
    SolarSystem* SS;
public:
    EditSolarSystemUI();
    ~EditSolarSystemUI();

    void menu(SolarSystemList &solar);
    void showList(SolarSystemList &solar);
    void showInfoSS(int index);

    void showNaturalBody();
    void addNewNaturalBody();

    void showManMadeObject();
    void addNewManMadeObject();

    void setMaxSS(int);
    int getMaxSS();

    void setSS(SolarSystem* solar);
    SolarSystem* getSS();


};

#endif	/* EDITSOLARSYSTEM_H */

