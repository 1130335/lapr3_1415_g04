#include "EditSimulationController.h"

EditSimulationController::EditSimulationController(){
}

EditSimulationController::~EditSimulationController(){
}


void EditSimulationController::chooseSimulation(int i, SimulationList& ls) {
    int cont = 1;
    list<Simulation*> aux = ls.getSimulationList();
    
    while(cont != i) {
        aux.pop_front();
        cont++;
    }
    
    chosen = aux.front();
}

void EditSimulationController::setAttributes(string name, double st, double et, double ts) {
    chosen->setName(name);
    chosen->setStart_Time(st);
    chosen->setEnd_Time(et);
    chosen->setTime_Step(ts);
}






