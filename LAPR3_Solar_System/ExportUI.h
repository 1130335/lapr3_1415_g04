/* 
 * File:   ExportUI.h
 * Author: Jony
 *
 * Created on 16 de Janeiro de 2015, 19:09
 */

#ifndef EXPORTUI_H
#define	EXPORTUI_H

#include "ExportController.h"
#include "SimulationList.h"
#include "ExportHTML.h"
#include <string>
#include <iostream>
#include <sstream>

using namespace std;

class ExportUI {

private:
    ExportController exc;
    ExportHTML exhtml;
    SimulationList sl;
    
public:
    ExportUI();
    ~ExportUI();
    
    void run(SimulationList&);
    
};


#endif	/* EXPORTUI_H */

