#ifndef ASTEROID_H
#define	ASTEROID_H

#include "NaturalObjects.h"
#include "Moon.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Asteroid : public NaturalObjects {
private:

    double totalSize; //em centimetros

public:
    vector<Moon*> moons;
    Asteroid();
    Asteroid(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string, double);
    ~Asteroid();
    Asteroid(Asteroid& a);
    void write(ostream& out);

    void setTotalSize(double t);

    double getTotalSize() const;

    void addMoons(Moon*);
    void showMoons();

};

#endif