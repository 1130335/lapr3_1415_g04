#ifndef SPACESHIP_H
#define	SPACESHIP_H

#include "Objects.h"
#include <string>

using namespace std;

class SpaceShip: public Objects{
    
private:
    
public:
    SpaceShip();
    SpaceShip(double v, double m, double g, double o, int id);
    ~SpaceShip();
    SpaceShip(const SpaceShip& sp);
    void write(ostream& out) const;
};

#endif	/* SPACESHIP_H */