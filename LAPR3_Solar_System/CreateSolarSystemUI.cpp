#include "CreateSolarSystemUI.h"

CreateSolarSystemUI::CreateSolarSystemUI() {

}

CreateSolarSystemUI::~CreateSolarSystemUI() {

}

void CreateSolarSystemUI::run(SolarSystemList& ssl) {
    cout << "\nCREATING SOLAR SYSTEM FROM SCRATCH" << endl;
    createStar();

    int bodies;
    cout << "Do you wish to create bodies in your new solar system?" << endl << "0 - no" << endl << "1 - yes" << endl;
    cout << endl << "Answer: ";
    cin >> bodies;
    while (bodies < 0 || bodies > 1) {
        cout << "Your answer was invalid.\nChoose 0 to continue the creation of the solar system without bodies or 1 if you wish to create a body." << endl;
        cout << endl << "Answer: ";
        cin >> bodies;
    }

    while (bodies == 1) {
        createNaturalObjects();
        cout << "Do you wish to create more bodies? \n0 - No \n1 - Yes \n\nAnswer: ";
        cin >> bodies;

        while (bodies < 0 || bodies > 1) {
            cout << "\nYour answer was invalid.\nChoose 0 to continue the creation of the solar system without bodies or 1 if you wish to create a body." << endl;
            cout << endl << "Answer: ";
            cin >> bodies;
        }
    }

    int objects;
    cout << "\nDo you wish to create man-made objects in this solar system? \n0 - No \n1 - Yes \n\nAnswer: ";
    cin >> objects;
    while (objects < 0 || objects > 1) {
        cout << "\nYour answer was invalid. Please choose 0 (no) or 1 (yes). \n\nAnswer: ";
        cin >> objects;
    }

    while (objects == 1) {
        createManMadeObjects();
        cout << "\nDo you wish to create more objects? \n0 - No \n1 - Yes \n\nAnswer: ";
        cin >> objects;

        while (objects < 0 || objects > 1) {
            cout << "\nYour answer was invalid. Please choose 0 (no) or 1 (yes). \n\nAnswer: ";
            cin >> objects;
        }
    }
    
    cssc.addSSToList(ssl);
    cout << "\nThis Solar System is now available on the list of Solar Systems.\n";
    
    cssc.createCSVFile();
    cout << "\nA CSV file was created with all the information about the Solar System created.\n";
    
    cout << "\nSOLAR SYSTEM SUCCESSFULLY CREATED" << endl;
}

void CreateSolarSystemUI::createStar() {
    cout << "Creating Solar System's Star..." << endl;
    string name;
    double mass, diameter;
    cout << "Name: ";
    cin >> name;
    cout << "Mass: ";
    cin >> mass;
    cout << "Equatorial Diameter: ";
    cin >> diameter;
    cout << endl;

    cssc.createStar(name, mass, diameter);
}

void CreateSolarSystemUI::createNaturalObjects() {
    int b_type;
    cout << "\nChoose one of the following types of bodies:" << endl;
    cout << "1 - Planet" << endl;
    cout << "2 - Minor Planer" << endl;
    cout << "3 - Asteroid" << endl;
    cout << "4 - Fireball" << endl;
    cout << "5 - Meteoroid" << endl;
    cout << "6 - Body" << endl;
    cout << "7 - Bolide" << endl;
    cout << "8 - Meteor" << endl;
    cout << "9 - Meteorite" << endl;
    cout << "10 - Comet" << endl;
    cout << "11 - Moon for last created body" << endl;
    cout << endl << "Answer: ";
    cin >> b_type;

    while (b_type < 1 || b_type > 11) {
        cout << "\nYour answer was invalid. Choose a number from 1 to 11 to create a body from a determinate type." << endl;
        cout << endl << "Answer: ";
        cin >> b_type;
    }

    while (b_type == 11 && cssc.nObjNotEmpty() == false) {
        cout << "\nYou need to create a body before creating a moon. Please choose a number between 1 and 10." << endl;
        cout << endl << "Answer: ";
        cin >> b_type;
    }

    switch (b_type) {
        case 1:
            cout << "\nCreating a planet..." << endl;
            getBodyData();
            cssc.createPlanet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nPlanet " << n << " created successfully.\n" << endl;
            break;

        case 2:
            cout << "\nCreating a minor planet..." << endl;
            getBodyData();
            cssc.createMinorPlanet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nMinor Planet " << n << " created successfully." << endl;
            break;

        case 3:
            cout << "\nCreating an asteroid... " << endl;
            getBodyData();
            cout << "Total Size: " << endl;
            double ts;
            cin >> ts;
            cssc.createAsteroid(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o, ts);
            cout << "\nAsteroid " << n << " created successfully." << endl;
            break;

        case 4:
            cout << "\nCreating a fireball..." << endl;
            getBodyData();
            cssc.createFireball(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nFireball " << n << " created successfully.";
            break;

        case 5:
            cout << "\nCreating a meteoroid..." << endl;
            getBodyData();
            while (ed < 0.0000010) {
                cout << "The equatorial diameter of the meteoroid needs to be bigger than 0.0000010. Please enter a new diameter." << endl;
                cout << "New Equatorial Diameter: ";
                cin >> ed;
            }
            cssc.createMeteoroid(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nMeteoroid " << n << " created successfully.";
            break;

        case 6:
            cout << "\nCreating a body..." << endl;
            getBodyData();
            cssc.createBody(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nBody " << n << " created successfully." << endl;
            break;

        case 7:
            cout << "\nCreating a bolide..." << endl;
            getBodyData();
            cssc.createBolide(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nBolide " << n << " created successfully." << endl;
            break;

        case 8:
            cout << "\nCreating a meteor..." << endl;
            getBodyData();
            cssc.createMeteor(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nMeteor " << n << " created successfully." << endl;
            break;

        case 9:
            cout << "\nCreating a meteorite..." << endl;
            getBodyData();
            cssc.createMeteorite(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nMeteorite " << n << " created successfully." << endl;
            break;

        case 10:
            cout << "\nCreating a comet..." << endl;
            getBodyData();
            double eo;
            cout << "Elliptical Orbit: ";
            cin >> eo;
            cssc.createComet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, eo, o);
            cout << "\nComet " << n << " created successfully." << endl;
            break;
        case 11:
            cout << "\nCreating a moon..." << endl;
            getBodyData();
            cssc.createMoon(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o);
            cout << "\nMoon " << n << " created successfully." << endl;
            break;
    }
}

void CreateSolarSystemUI::createManMadeObjects() {
    int o_type;
    cout << "\nChoose one of the following type of objects: " << endl;
    cout << "1 - Satellite" << endl;
    cout << "2 - Space-ship \n\nAnswer: ";
    cin >> o_type;

    while (o_type < 1 || o_type > 2) {
        cout << "\n\nYour answer was invalid. Please choose on of the following options: " << endl;
        cout << "1 - Satellite" << endl;
        cout << "2 - Space-ship \nAnswer: " << endl;
        cin >> o_type;
    }

    switch (o_type) {
        case 1:
            cout << "\nCreating a satellite... " << endl;
            getObjectData();
            cssc.createSatellite(mass, orbit, velocity, force, id);
            cout << "nSatellite created successfully." << endl;
            break;

        case 2:
            cout << "\nCreating a space-ship... " << endl;
            getObjectData();
            cssc.createSpaceShip(mass, orbit, velocity, force, id);
            cout << "\nSpace-Ship created successfully." << endl;
            break;
    }

}

void CreateSolarSystemUI::getObjectData() {
    cout << "Man-Made Object Characteristics..." << endl;
    cout << "Mass: ";
    cin >> mass;
    cout << "Velocity: ";
    cin >> velocity;
    cout << "Orbit: ";
    cin >> orbit;
    cout << "Gravitational Force: ";
    cin >> force;
    cout << "Objects's ID: ";
    cin >> id;
}

void CreateSolarSystemUI::getBodyData() {
    cout << "Natural Object Characteristics..." << endl;
    cout << "Name: ";
    cin >> n;
    cout << "Orbiting: ";
    cin >> o;
    cout << "Period of Revolution: ";
    cin >> pr;
    cout << "Orbital Speed: ";
    cin >> os;
    cout << "Inclination of Axis to Orbit: ";
    cin >> iao;
    cout << "Equatorial Diameter: ";
    cin >> ed;
    cout << "Mass: ";
    cin >> m;
    cout << "Density: ";
    cin >> d;
    cout << "Escape Velocity: ";
    cin >> ev;
    cout << "Semimajor Axis: ";
    cin >> sa;
    cout << "Orbit Eccentricity: ";
    cin >> e;
    cout << "Orbit Inclination: ";
    cin >> oi;
    cout << "Perihelion: ";
    cin >> p;
    cout << "Aphelion: ";
    cin >> a;
    cout << "Px: ";
    cin >> px;
    cout << "Py: ";
    cin >> py;
    cout << "Pz: ";
    cin >> pz;
    cout << "Vx: ";
    cin >> vx;
    cout << "Vy: ";
    cin >> vy;
    cout << "Vz: ";
    cin >> vz;
}

