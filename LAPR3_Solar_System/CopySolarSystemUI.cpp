#include "CopySolarSystemUI.h"

CopySolarSystemUI::CopySolarSystemUI() {
    
}

CopySolarSystemUI::~CopySolarSystemUI() {
    
}

void CopySolarSystemUI::run(SolarSystemList& ssl) {
    cout << "\nCOPY SOLAR SYSTEM" << endl;
    cout << "Presenting the most recent list of solar systems..." << endl;
    cssc.showList(ssl);
    cout << "\nPlease choose one of the previous systems. \n\nAnswer: ";
    int s;
    cin >> s;
    SolarSystem* ss = (cssc.getSolarSystem(s,ssl));
    cout << endl << *ss;
    cout << endl << "Copying chosen Solar System...\n";
    cssc.copySolarSystem(ss, ssl);
    cout << endl << "List updated..." << endl;
    cssc.showList(ssl);
    cout << endl << "SOLAR SYSTEM COPIED SUCCESSFULLY" << endl << endl;
}
