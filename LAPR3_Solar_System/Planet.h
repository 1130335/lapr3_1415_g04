#ifndef PLANET_H
#define	PLANET_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Planet : public NaturalObjects {
private:
    vector<Moon*> moons;
public:
    Planet();
    Planet(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    Planet(Planet&);
    ~Planet();
    void write(ostream&);

    void addMoons(Moon*);
    void showMoons();
};

#endif	/* PLANET_H */

