#ifndef SIMULATIONLIST_H
#define	SIMULATIONLIST_H

#include <list>
#include <string>
#include <iostream>
#include "Simulation.h"

using namespace std;

class SimulationList {
public:
    
    list<Simulation*> sl;
    
    SimulationList();
    SimulationList(list<Simulation*> l);
    virtual ~SimulationList();
    
    void add(Simulation* s);
    void remove();
    void showSimulations(ostream&);
    
    list<Simulation*> getSimulationList() const;
    void setSimulationList(list<Simulation*> simList);
    friend ostream & operator<<(ostream &out, SimulationList& l);
    
private:

};

#endif	/* SIMULATIONLIST_H */

