/*
 * File:   Star_Test.h
 * Author: Rita
 *
 * Created on 15/jan/2015, 21:04:59
 */

#ifndef STAR_TEST_H
#define	STAR_TEST_H

#include <cppunit/extensions/HelperMacros.h>

class Star_Test : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(Star_Test);

    CPPUNIT_TEST(testGetEquatorialDiameter);
    CPPUNIT_TEST(testGetMass);
    CPPUNIT_TEST(testGetName);

    CPPUNIT_TEST_SUITE_END();

public:
    Star_Test();
    virtual ~Star_Test();
    void setUp();
    void tearDown();

private:
    void testGetEquatorialDiameter();
    void testGetMass();
    void testGetName();

};

#endif	/* STAR_TEST_H */

