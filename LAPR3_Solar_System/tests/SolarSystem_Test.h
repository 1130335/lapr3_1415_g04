/*
 * File:   SolarSystem_Test.h
 * Author: Rita
 *
 * Created on 15/jan/2015, 21:13:15
 */

#ifndef SOLARSYSTEM_TEST_H
#define	SOLARSYSTEM_TEST_H

#include <cppunit/extensions/HelperMacros.h>

using namespace std;

class SolarSystem_Test : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(SolarSystem_Test);

    CPPUNIT_TEST(testAddToNaturalObj);
    CPPUNIT_TEST(testAddToObjects);
    CPPUNIT_TEST(testGetNObj);
    CPPUNIT_TEST(testGetName);
    CPPUNIT_TEST(testGetObj);
    CPPUNIT_TEST(testGetStar);

    CPPUNIT_TEST_SUITE_END();

public:
    SolarSystem_Test();
    virtual ~SolarSystem_Test();
    void setUp();
    void tearDown();

private:
    void testAddMoontoBody();
    void testAddToNaturalObj();
    void testAddToObjects();
    void testGetNObj();
    void testGetName();
    void testGetObj();
    void testGetStar();
    void testRemoveNaturalObj();
    void testRemoveObjects();
    void testSetName();

};

#endif	/* SOLARSYSTEM_TEST_H */

