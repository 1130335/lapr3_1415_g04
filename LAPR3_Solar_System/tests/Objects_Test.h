/*
 * File:   Objects_Test.h
 * Author: Rita
 *
 * Created on 14/jan/2015, 16:25:25
 */

#ifndef OBJECTS_TEST_H
#define	OBJECTS_TEST_H

#include <cppunit/extensions/HelperMacros.h>

class Objects_Test : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(Objects_Test);

    CPPUNIT_TEST(testGetGraForce);
    CPPUNIT_TEST(testGetID);
    CPPUNIT_TEST(testGetMass);
    CPPUNIT_TEST(testGetOrbit);
    CPPUNIT_TEST(testGetVelocity);

    CPPUNIT_TEST_SUITE_END();

public:
    Objects_Test();
    virtual ~Objects_Test();
    void setUp();
    void tearDown();

private:
    void testGetGraForce();
    void testGetID();
    void testGetMass();
    void testGetOrbit();
    void testGetVelocity();

};

#endif	/* OBJECTS_TEST_H */

