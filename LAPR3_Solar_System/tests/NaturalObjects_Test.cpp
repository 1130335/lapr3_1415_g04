#include "NaturalObjects_Test.h"
#include "../NaturalObjects.h"

CPPUNIT_TEST_SUITE_REGISTRATION(NaturalObjects_Test);

NaturalObjects_Test::NaturalObjects_Test() {
}

NaturalObjects_Test::~NaturalObjects_Test() {
}

void NaturalObjects_Test::setUp() {
}

void NaturalObjects_Test::tearDown() {
}

void NaturalObjects_Test::testGetAphelion() {
    NaturalObjects naturalObjects;
    naturalObjects.setAphelion(1);
    double result = naturalObjects.getAphelion();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetMeanAnomaly() {
    NaturalObjects naturalObjects;
    naturalObjects.setMeanAnomaly(1);
    double result = naturalObjects.getMeanAnomaly();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetLongitudeP() {
    NaturalObjects naturalObjects;
    naturalObjects.setLongitudeP(1);
    double result = naturalObjects.getLongitudeP();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetLongitudeAsc() {
    NaturalObjects naturalObjects;
    naturalObjects.setLongitudeAsc(1);
    double result = naturalObjects.getLongitudeAsc();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetDensity() {
    NaturalObjects naturalObjects;
    naturalObjects.setDensity(1);
    double result = naturalObjects.getDensity();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetEccentricity() {
    NaturalObjects naturalObjects;
    naturalObjects.setEccentricity(1);
    double result = naturalObjects.getEccentricity();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetEquatorialDiameter() {
    NaturalObjects naturalObjects;
    naturalObjects.setEquatorialDiameter(1);
    double result = naturalObjects.getEquatorialDiameter();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetEscapeVelocity() {
    NaturalObjects naturalObjects;
    naturalObjects.setEscapeVelocity(1);
    double result = naturalObjects.getEscapeVelocity();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetInclinationAxisOrbit() {
    NaturalObjects naturalObjects;
    naturalObjects.setInclinationAxisOrbit(1);
    double result = naturalObjects.getInclinationAxisOrbit();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetMass() {
    NaturalObjects naturalObjects;
    naturalObjects.setMass(1);
    double result = naturalObjects.getMass();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetName() {
    NaturalObjects naturalObjects;
    naturalObjects.setName("Name");
    string result = naturalObjects.getName();
    string expected = "Name";
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetOrbitInclination() {
    NaturalObjects naturalObjects;
    naturalObjects.setOrbitInclination(1);
    double result = naturalObjects.getOrbitInclination();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetOrbitalSpeed() {
    NaturalObjects naturalObjects;
    naturalObjects.setOrbitalSpeed(1);
    double result = naturalObjects.getOrbitalSpeed();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetOrbiting() {
    NaturalObjects naturalObjects;
    naturalObjects.setOrbiting("Star");
    string result = naturalObjects.getOrbiting();
    string expected = "Star";
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetPerihelion() {
    NaturalObjects naturalObjects;
    naturalObjects.setPerihelion(1);
    double result = naturalObjects.getPerihelion();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetPeriodRevolution() {
    NaturalObjects naturalObjects;
    naturalObjects.setPeriodRevolution(1);
    double result = naturalObjects.getPeriodRevolution();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetPx() {
    NaturalObjects naturalObjects;
    naturalObjects.setPx(1);
    double result = naturalObjects.getPx();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetPy() {
    NaturalObjects naturalObjects;
    naturalObjects.setPy(1);
    double result = naturalObjects.getPy();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetPz() {
    NaturalObjects naturalObjects;
    naturalObjects.setPz(1);
    double result = naturalObjects.getPz();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetSemiMajorAxis() {
    NaturalObjects naturalObjects;
    naturalObjects.setSemiMajorAxis(1);
    double result = naturalObjects.getSemiMajorAxis();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetVx() {
    NaturalObjects naturalObjects;
    naturalObjects.setVx(1);
    double result = naturalObjects.getVx();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetVy() {
    NaturalObjects naturalObjects;
    naturalObjects.setVy(1);
    double result = naturalObjects.getVy();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void NaturalObjects_Test::testGetVz() {
    NaturalObjects naturalObjects;
    naturalObjects.setVz(1);
    double result = naturalObjects.getVz();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

