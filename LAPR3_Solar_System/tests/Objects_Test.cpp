#include "Objects_Test.h"
#include "../Objects.h"


CPPUNIT_TEST_SUITE_REGISTRATION(Objects_Test);

Objects_Test::Objects_Test() {
}

Objects_Test::~Objects_Test() {
}

void Objects_Test::setUp() {
}

void Objects_Test::tearDown() {
}

void Objects_Test::testGetGraForce() {
    Objects objects;
    objects.setGraForce(1);
    double result = objects.getGraForce();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Objects_Test::testGetID() {
    Objects objects;
    objects.setID(1);
    double result = objects.getID();
    int expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Objects_Test::testGetMass() {
    Objects objects;
    objects.setMass(1);
    double result = objects.getMass();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Objects_Test::testGetOrbit() {
    Objects objects;
    objects.setOrbit(1);
    double result = objects.getOrbit();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Objects_Test::testGetVelocity() {
    Objects objects;
    objects.setVelocity(1);
    double result = objects.getVelocity();
    double expected = 1;
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

