#include "SolarSystem_Test.h"
#include "../SolarSystem.h"


CPPUNIT_TEST_SUITE_REGISTRATION(SolarSystem_Test);

SolarSystem_Test::SolarSystem_Test() {
}

SolarSystem_Test::~SolarSystem_Test() {
}

void SolarSystem_Test::setUp() {
}

void SolarSystem_Test::tearDown() {
}

void SolarSystem_Test::testAddToNaturalObj() {
    NaturalObjects* p0 = new NaturalObjects();
    SolarSystem solarSystem;
    solarSystem.addToNaturalObj(p0);
    if (solarSystem.getNObj().back()->getName()== p0->getName()) {
        CPPUNIT_ASSERT(true);
    }
}

void SolarSystem_Test::testAddToObjects() {
    Objects* p0 = new Objects();
    SolarSystem solarSystem;
    solarSystem.addToObjects(p0);
    if (solarSystem.getObj().back()->getID() == p0->getID()) {
        CPPUNIT_ASSERT(true);
    }
}

void SolarSystem_Test::testGetNObj() {
    SolarSystem solarSystem;
    list<NaturalObjects*> l;
    solarSystem.setNObj(l);
    list<NaturalObjects*> result = solarSystem.getNObj();
    if (result.size() == l.size()) {
        CPPUNIT_ASSERT(true);
    }
}

void SolarSystem_Test::testGetName() {
    SolarSystem solarSystem;
    solarSystem.setName("Name");
    string result = solarSystem.getName();
    if (result == "Name") {
        CPPUNIT_ASSERT(true);
    }
}

void SolarSystem_Test::testGetObj() {
    SolarSystem solarSystem;
    list<Objects*> l;
    solarSystem.setObj(l);
    list<Objects*> result = solarSystem.getObj();
    if (result.size() == l.size()) {
        CPPUNIT_ASSERT(true);
    }
}

void SolarSystem_Test::testGetStar() {
    SolarSystem solarSystem;
    Star s;
    solarSystem.setStar(s);
    Star& result = solarSystem.getStar();
    if (result.getName() == s.getName()) {
        CPPUNIT_ASSERT(true);
    }
}

