#ifndef NATURALOBJECTS_TEST_H
#define	NATURALOBJECTS_TEST_H

#include <cppunit/extensions/HelperMacros.h>
#include <string>

using namespace std;

class NaturalObjects_Test : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(NaturalObjects_Test);

    CPPUNIT_TEST(testGetAphelion);
    CPPUNIT_TEST(testGetDensity);
    CPPUNIT_TEST(testGetEccentricity);
    CPPUNIT_TEST(testGetEquatorialDiameter);
    CPPUNIT_TEST(testGetEscapeVelocity);
    CPPUNIT_TEST(testGetInclinationAxisOrbit);
    CPPUNIT_TEST(testGetMass);
    CPPUNIT_TEST(testGetName);
    CPPUNIT_TEST(testGetOrbitInclination);
    CPPUNIT_TEST(testGetOrbitalSpeed);
    CPPUNIT_TEST(testGetOrbiting);
    CPPUNIT_TEST(testGetPerihelion);
    CPPUNIT_TEST(testGetPeriodRevolution);
    CPPUNIT_TEST(testGetPx);
    CPPUNIT_TEST(testGetPy);
    CPPUNIT_TEST(testGetPz);
    CPPUNIT_TEST(testGetSemiMajorAxis);
    CPPUNIT_TEST(testGetVx);
    CPPUNIT_TEST(testGetVy);
    CPPUNIT_TEST(testGetVz);
    CPPUNIT_TEST(testGetMeanAnomaly);
    CPPUNIT_TEST(testGetLongitudeP);
    CPPUNIT_TEST(testGetLongitudeAsc);

    CPPUNIT_TEST_SUITE_END();

public:
    NaturalObjects_Test();
    virtual ~NaturalObjects_Test();
    void setUp();
    void tearDown();

private:
    void testGetAphelion();
    void testGetDensity();
    void testGetEccentricity();
    void testGetEquatorialDiameter();
    void testGetEscapeVelocity();
    void testGetInclinationAxisOrbit();
    void testGetMass();
    void testGetName();
    void testGetOrbitInclination();
    void testGetOrbitalSpeed();
    void testGetOrbiting();
    void testGetPerihelion();
    void testGetPeriodRevolution();
    void testGetPx();
    void testGetPy();
    void testGetPz();
    void testGetSemiMajorAxis();
    void testGetVx();
    void testGetVy();
    void testGetVz();
    void testGetMeanAnomaly();
    void testGetLongitudeP();
    void testGetLongitudeAsc();

};

#endif	/* NATURALOBJECTS_TEST_H */

