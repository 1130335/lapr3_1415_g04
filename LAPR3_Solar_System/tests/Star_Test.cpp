#include "Star_Test.h"
#include "../Star.h"


CPPUNIT_TEST_SUITE_REGISTRATION(Star_Test);

Star_Test::Star_Test() {
}

Star_Test::~Star_Test() {
}

void Star_Test::setUp() {
}

void Star_Test::tearDown() {
}

void Star_Test::testGetEquatorialDiameter() {
    Star star;
    star.setEquatorialDiameter(1);
    double expected = 1;
    double result = star.getEquatorialDiameter();
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Star_Test::testGetMass() {
    Star star;
    star.setMass(1);
    double expected = 1;
    double result = star.getMass();
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

void Star_Test::testGetName() {
    Star star;
    star.setName("Name");
    string expected = "Name";
    string result = star.getName();
    if (result == expected) {
        CPPUNIT_ASSERT(true);
    }
}

