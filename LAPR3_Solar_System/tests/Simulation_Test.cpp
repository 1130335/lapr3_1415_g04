#include "Simulation_Test.h"
#include "../Simulation.h"


CPPUNIT_TEST_SUITE_REGISTRATION(Simulation_Test);

Simulation_Test::Simulation_Test() {
}

Simulation_Test::~Simulation_Test() {
}

void Simulation_Test::setUp() {
}

void Simulation_Test::tearDown() {
}

void Simulation_Test::testGetEnd_Time() {
    Simulation simulation;
    simulation.setEnd_time(1);
    double result = simulation.getEnd_Time();
    if (result == 1) {
        CPPUNIT_ASSERT(true);
    }
}

void Simulation_Test::testGetName() {
    Simulation simulation;
    simulation.setName("Name");
    string result = simulation.getName();
    if (result == "Name") {
        CPPUNIT_ASSERT(true);
    }
}

void Simulation_Test::testGetSolar() {
    Simulation simulation;
    SolarSystem* ss = new SolarSystem();
    simulation.setSolarSystem(ss);
    SolarSystem* result = simulation.getSolar();
    if (ss->getName() == result->getName()) {
        CPPUNIT_ASSERT(true);
    }
}

void Simulation_Test::testGetStart_Time() {
    Simulation simulation;
    simulation.setStart_time(1);
    double result = simulation.getStart_Time();
    if (result == 1) {
        CPPUNIT_ASSERT(true);
    }
}

void Simulation_Test::testGetTime_Step() {
    Simulation simulation;
    simulation.setTime_Step(1);
    double result = simulation.getTime_Step();
    if (result == 1) {
        CPPUNIT_ASSERT(true);
    }
}


