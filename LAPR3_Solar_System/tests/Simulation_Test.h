/*
 * File:   Simulation_Test.h
 * Author: Rita
 *
 * Created on 15/jan/2015, 21:52:45
 */

#ifndef SIMULATION_TEST_H
#define	SIMULATION_TEST_H

#include <cppunit/extensions/HelperMacros.h>

class Simulation_Test : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(Simulation_Test);

    CPPUNIT_TEST(testGetEnd_Time);
    CPPUNIT_TEST(testGetName);
    CPPUNIT_TEST(testGetSolar);
    CPPUNIT_TEST(testGetStart_Time);
    CPPUNIT_TEST(testGetTime_Step);

    CPPUNIT_TEST_SUITE_END();

public:
    Simulation_Test();
    virtual ~Simulation_Test();
    void setUp();
    void tearDown();

private:
    void testGetEnd_Time();
    void testGetEnd_Time2();
    void testGetName();
    void testGetSolar();
    void testGetStart_Time();
    void testGetStart_Time2();
    void testGetTime_Step();
    void testGetTime_Step2();
    void testGetVNObj();

};

#endif	/* SIMULATION_TEST_H */

