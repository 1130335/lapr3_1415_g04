#ifndef EDITSIMULATIONUI_H
#define	EDITSIMULATIONUI_H

#include <iostream>
#include <string>
#include "SimulationList.h"
#include "EditSimulationController.h"

using namespace std;

class EditSimulationUI {
public:
    EditSimulationUI();
    virtual ~EditSimulationUI();
    
    void run(SimulationList&);
private:
    EditSimulationController esc;
};

#endif	/* EDITSIMULATIONUI_H */

