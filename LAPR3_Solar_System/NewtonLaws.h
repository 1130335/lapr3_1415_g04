#ifndef NEWTONLAWS_H
#define	NEWTONLAWS_H

class NewtonLaws {
public:
   
    NewtonLaws();
    NewtonLaws(const NewtonLaws& orig);
    ~NewtonLaws();
    
    double velocityLaw(double, double, double, double);
    double positionLaw(double, double, double, double, double);
    
private:

};

#endif	/* NEWTONLAWS_H */

