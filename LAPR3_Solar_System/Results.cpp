#include "Results.h"

Results::Results(){
    
}
Results::~Results(){
    
}
vector<double> Results::getXVel(){
    return this->xVel;
}

vector<double> Results::getYVel(){
    return this->yVel;
}

vector<double> Results::getZVel(){
    return this->zVel;
}

vector<double> Results::getXPos(){
    return this->getXPos();
    
}

vector<double> Results::getYPos(){
    return this->yPos;
}

vector<double> Results::getZPos(){
    return this->zPos;
}

void Results::setXVel(vector<double> xV){
    this->xVel=xV;
}

void Results::setYVel(vector<double> yV){
    this->yVel=yV;
}

void Results::setZVel(vector<double> zV){
    this->zVel=zV;
}

void Results::setXPos(vector<double> xP){
    this->xPos=xP;
}

void Results::setYPos(vector<double> yP){
    this->yPos=yP;
}

void Results::setZPos(vector<double> zP){
    this->zPos=zP;
}

void Results::setTimestep(vector<double> ts){
    this->timestep=ts;
}

void Results::inserirXpos(double xP){
    this->xPos.push_back(xP);
}

void Results::inserirYpos(double yP){
    this->yPos.push_back(yP);
}

void Results::inserirZpos(double zP){
    this->zPos.push_back(zP);
}

void Results::inserirXvel(double xV){
    this->xVel.push_back(xV);
}

void Results::inserirYvel(double yV){
    this->yVel.push_back(yV);
}

void Results::inserirZvel(double zV){
    this->zVel.push_back(zV);
}

void Results::inserirTimeStep(double ts){
    this->timestep.push_back(ts);
}



