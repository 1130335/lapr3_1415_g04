#include "NewtonLaws.h"

NewtonLaws::NewtonLaws() {
}

NewtonLaws::NewtonLaws(const NewtonLaws& orig) {
}

NewtonLaws::~NewtonLaws() {
}

double NewtonLaws::velocityLaw(double v, double f, double mass, double t) {

double vel;

vel = v + ((f / mass)*t);

return vel;

}

double NewtonLaws::positionLaw(double p, double f, double v, double mass, double t) {

double position;

position = p + t*(v + (t*0.5 / mass)*f);
return position;

}

