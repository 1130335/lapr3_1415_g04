#ifndef MOON_H
#define	MOON_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Moon : public NaturalObjects {
public:
    vector<Moon*> moons;
    Moon();
    Moon(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    ~Moon();
    Moon(Moon& m);
    void write(ostream&);

    void addMoons(Moon*);
    void showMoons();

};

#endif