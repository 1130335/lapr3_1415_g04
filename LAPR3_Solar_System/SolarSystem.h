#ifndef SOLARSYSTEM_H
#define	SOLARSYSTEM_H

#include "NaturalObjects.h"
#include "Moon.h"
#include "Asteroid.h"
#include "Body.h"
#include "Bolide.h"
#include "Comet.h"
#include "Fireball.h"
#include "Meteor.h"
#include "Meteorite.h"
#include "Meteoroid.h"
#include "MinorPlanet.h"
#include "Planet.h"
#include "Objects.h"
#include "Satellite.h"
#include "SpaceShip.h"
#include "Star.h"
#include <string>
#include <iostream>
#include <list>
#include <typeinfo>

class SolarSystem {
private:
    string name;
    list<Objects*> obj;
    list<NaturalObjects*> nObj;
    Star system_star;

public:   
    SolarSystem();
    SolarSystem(Star& s);
    ~SolarSystem();
    SolarSystem(const SolarSystem&);
    void setName(string);
    string getName();
    
    list<NaturalObjects*> getNObj();
    void setNObj(list<NaturalObjects*> newnObj);
    
    void setObj(list<Objects*> newObj);
    list<Objects*> getObj();
    
    void showInfo(ostream&);
    void setStar(Star&);
    Star& getStar();
    
    void addToNaturalObj(NaturalObjects*);
    void addMoontoBody(Moon*);
    void addToObjects(Objects*);
    void removeNaturalObj();
    void removeObjects();
    bool nObjNotEmpty();
};

ostream & operator<<(ostream &out, SolarSystem& ss);

#endif	

