#ifndef COPYSSCONTROLLER_FT_H
#define	COPYSSCONTROLLER_FT_H

/* Teste Funcional para CopySolarSystemController */

#include "CopySolarSystemController.h"
#include "SolarSystem.h"
#include "SolarSystemList.h"
#include <iostream>

using namespace std;

class CopySSController_FT {
public:
    CopySSController_FT();
    virtual ~CopySSController_FT();
    
    void run();
    
private:
    SolarSystem* ss1;
    SolarSystemList sslTest;
    CopySolarSystemController cssc;
};

#endif	/* COPYSSCONTROLLER_FT_H */

