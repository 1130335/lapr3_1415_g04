#ifndef FIREBALL_H
#define	FIREBALL_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Fireball : public NaturalObjects {
private:

public:
    vector<Moon*> moons;
    Fireball();
    Fireball(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    Fireball(Fireball& f);
    void write(ostream& out);

    void addMoons(Moon*);
    void showMoons();
};

#endif	/* FIREBALL_H */

