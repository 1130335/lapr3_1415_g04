#include "EditSimulationUI.h"
#include "EditSimulationController.h"

EditSimulationUI::EditSimulationUI() {
}

EditSimulationUI::~EditSimulationUI() {
}

void EditSimulationUI::run(SimulationList& sl) {
    int sim;
    double st, et, ts;
    string name;
    cout << "\nEDITING SIMULATION" << endl;
    cout << "Simulation List..." << endl;
    cout << sl;
    cout << "Choose one of the above. \nAnswer: " << endl;
    cin >> sim;
    esc.chooseSimulation(sim,sl);
    cout << "Enter the following field... " << endl;
    cout << "Name: ";
    cin >> name;
    cout << "Start Time: ";
    cin >> st;
    cout << "End Time: ";
    cin >> et;
    cout << "Time Step: ";
    cin >> ts;
    esc.setAttributes(name,st,et,ts);
    
    cout << "\nSIMULATION EDITED SUCCESSFULLY\n" << endl;
    
}