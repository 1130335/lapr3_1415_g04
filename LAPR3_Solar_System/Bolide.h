#ifndef BOLIDE_H
#define	BOLIDE_H

#include "NaturalObjects.h"
#include "Moon.h"
#include <vector>
#include <string>
#include <iostream>

using namespace std;

class Bolide : public NaturalObjects {
private:

public:
    vector<Moon*> moons;
    Bolide();
    Bolide(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    ~Bolide();
    Bolide(Bolide& b);
    void write(ostream& out);

    void addMoons(Moon*);
    void showMoons();

};

#endif	/* BOLIDE_H */

