#include "Asteroid.h"

Asteroid::Asteroid() : NaturalObjects() {
    this->totalSize = 10;
}

Asteroid::Asteroid(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o, double t) : NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz,o) {
    this->totalSize = t;
}

Asteroid::~Asteroid() {

}

Asteroid::Asteroid(Asteroid& a) : NaturalObjects(a) {
    this->totalSize = a.getTotalSize();
}

void Asteroid::setTotalSize(double t) {
    this->totalSize = t;
}

double Asteroid::getTotalSize() const {
    return this->totalSize;
}

void Asteroid::write(ostream& out) {
    out << "Body Type: Asteroid" << endl;
    out << "Total Size: " << this->getTotalSize() << " cm" << endl;
    NaturalObjects::write(out);
    out << endl;
}

void Asteroid::addMoons(Moon* m) {
    this->moons.push_back(m);
}

void Asteroid::showMoons() {
    for (int i = 0; i < this->moons.size(); i++) {
        cout << *this->moons[i];
    }
}
