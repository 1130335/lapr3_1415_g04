/* 
 * File:   OpenSolarSystemUI.h
 * Author: Asus
 *
 * Created on 17 de Dezembro de 2014, 17:31
 */

#ifndef OPENSOLARSYSTEMUI_H
#define	OPENSOLARSYSTEMUI_H

#include "OpenSolarSystemController.h"
#include "SolarSystemList.h"
#include "SolarSystem.h"
#include <iostream>

using namespace std;

class OpenSolarSystemUI {
private:
    OpenSolarSystemController abrirc;
    int maxSS;
    SolarSystem* SS;
public:
    OpenSolarSystemUI();
    ~OpenSolarSystemUI();
    
    void menu(SolarSystemList &solar);
    void showList(SolarSystemList &solar);
    void showInfoSS(int index);
    
    void setSS(SolarSystem* solar);
    SolarSystem* getSS();

	void setMaxSS(int);
	int getMaxSS();
    
};

#endif	/* OPENSOLARSYSTEMUI_H */

