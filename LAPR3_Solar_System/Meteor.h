#ifndef METEOR_H
#define	METEOR_H

#include <vector>
#include <string>
#include "NaturalObjects.h"
#include "Moon.h"

using namespace std;

class Meteor : public NaturalObjects {
private:

public:
    vector<Moon*> moons;

    Meteor();
    Meteor(string, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, double, string);
    Meteor(Meteor& m);
    void write(ostream& out);

    void addMoons(Moon*);
    void showMoons();

};

#endif	/* METEOR_H */

