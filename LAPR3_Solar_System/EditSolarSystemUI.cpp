#include "EditSolarSystemUI.h"

EditSolarSystemUI::EditSolarSystemUI() {

}

EditSolarSystemUI::~EditSolarSystemUI() {

}

void EditSolarSystemUI::menu(SolarSystemList &solar) {
    int opcao = -1;
    int opcaoo = -1;
    int SSchosen = -1;
    //bool flag = false;
    editc.setLSS(solar.getSSL());
    do {
        cout << "What action you want to perform? \n\t1 - List all Solar Systems \n\t0 - Exit\n" << endl;
        cin >> opcao;
        if (opcao == 1) {
            do {
                cout << "List of all the Solar System! " << endl;
                showList(solar);
                cout << "What is the solar system you want to edit?\n\t0 - Exit " << endl;
                cin >> SSchosen;
                if (SSchosen == 0) {
                    cout << "Exit edition of the solar system! " << endl;
                } else if (SSchosen > maxSS || SSchosen < 0) {
                    cout << "The value entered does not match any valid Solar System, please enter a valid value! " << endl;
                } else {
                    showInfoSS(SSchosen); 
                    do {
                        cout << "What solar system object you want to edit? \n\t1 - Natural bodies \n\t2 - Man made object \n\t0 - Exit" << endl;
                        cin >> opcaoo;
                        if (opcaoo == 1) {
                            int chosen = -1;
                            do {
                                cout << "What action you want to perform?  \n\t1 - Add Natural Bodies \n\t2 - Remove Natural Bodies \n\t0 - Exit" << endl;
                                cin >> chosen;
                                if (chosen == 1) {
                                    cout << "Starting with the creation of a new natural body! \n" << endl;
                                    addNewNaturalBody();
                                    cout << "Addition of new natural body was successful!" << endl;
                                } else if (chosen == 2) {
                                    int nbdelete;
                                    showNaturalBody();
                                    cout << "What Natural body do you want to delete? \n\t0 - Exit" << endl;
                                    cin >> nbdelete;
                                    if (nbdelete <= maxNB && nbdelete > 0) {
                                        editc.deleteNB(nbdelete);
                                        cout << "Natural body deleted successfully!" << endl;
                                    } else if (nbdelete == 0) {
                                        cout << "Successfully exit!" << endl;
                                    } else {
                                        cout << "There is no natural body associated with this value!" << endl;
                                    }
                                } else if (chosen == 0) {
                                    cout << "Successfully exit!" << endl;
                                } else {
                                    cout << "Enter one of the intended numbers!" << endl;
                                }
                            } while (chosen != 0);

                        } else if (opcaoo == 2) {
                            int chosen = -1;
                            do {
                                cout << "What action you want to perform?  \n\t1 - Add Man made object \n\t2 - Remove Man made object \n\t0 - Exit" << endl;
                                cin >> chosen;
                                if (chosen == 1) {
                                    cout << "Starting with the creation of a new Man made object! \n" << endl;
                                    addNewManMadeObject();
                                    cout << "Addition of new Man made object was successful!" << endl;
                                } else if (chosen == 2) {
                                    int mmodelete;
                                    showManMadeObject();
                                    cout << "What Man made object do you want to delete? \n\t0 - Exit" << endl;
                                    cin >> mmodelete;
                                    if (mmodelete <= maxMMO && mmodelete > 0) {
                                        editc.deleteMMO(mmodelete);
                                        cout << "Man made object deleted successfully!" << endl;
                                    } else if (mmodelete == 0) {
                                        cout << "Successfully exit!" << endl;
                                    } else {
                                        cout << "There is no Man made object associated with this value!" << endl;
                                    }

                                } else if (chosen == 0) {
                                    cout << "Successfully exit!" << endl;
                                } else {
                                    cout << "Enter one of the intended numbers!" << endl;
                                }
                            } while (chosen != 0);
                        } else if (opcao == 0) {
                            cout << "Successfully exit!" << endl;
                        } else {
                            cout << "Enter one of the intended numbers!" << endl;
                        }
                    } while (opcaoo != 0);
                }
            } while (SSchosen != 0);
        } else if (opcao == 0) {
            cout << "Exit edition of the solar system with success!" << endl;
        } else {
            cout << "Enter one of the intended numbers!" << endl;
        }
    } while (opcao != 0);
}

/**
 * Metodo para mostrar a lista de todos os sistemas solares
 * @param lss
 */
void EditSolarSystemUI::showList(SolarSystemList &solar) {
    setMaxSS(solar.getSSL().size());
    cout << solar << endl;
}

/**
 *  Metodod que escreve a informação relativa ao sistema solar escolhido pelo utilizador
 * recebendo por parametro o "index" ao qual o Sistema Solar escolhido esta na lista
 * @param index -> subtrair 1 para obter a possição certa na lista.
 */
void EditSolarSystemUI::showInfoSS(int index) {
    index = (index - 1);
    setSS(editc.getInfoSS(index));
    cout << *SS << endl;
}


/*
 *      ###### CORPOS NATURAIS ######
 */

/**
 * Metodo que vai mostrar a lista de corpos naturais
 */
void EditSolarSystemUI::showNaturalBody() {
    list<NaturalObjects*> aux;
    maxNB = editc.alterNaturalBody();
    aux = editc.getNB();
    list<NaturalObjects*>::iterator i;
    int cont = 0;
    for (i = aux.begin(); i != aux.end(); ++i) {
        cont++;
        cout << "\t" << cont << "- " << *i << endl;
        cout << "\n" << endl;
    }
}

void EditSolarSystemUI::addNewNaturalBody() {
    string n, o;
    float pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz;

    cout << "\t-Name: " << endl;
    cin >> n;
    cout << "\t-Px (AU): " << endl;
    cin >> px;
    cout << "\t-Py (AU): " << endl;
    cin >> py;
    cout << "\t-Pz (AU): " << endl;
    cin >> pz;
    cout << "\t-Vx (m/s): " << endl;
    cin >> vx;
    cout << "\t-Vy (m/s): " << endl;
    cin >> vy;
    cout << "\t-Vz (m/s): " << endl;
    cin >> vz;
    cout << "\t-Mass: " << endl;
    cin >> m;
    cout << "\t-Density: " << endl;
    cin >> d;
    cout << "\t-Eccentricity: " << endl;
    cin >> e;
    cout << "\t-Equatorial Diameter (km): " << endl;
    cin >> ed;
    cout << "\t-Inclination of Axis to Orbit: " << endl;
    cin >> iao;
    cout << "\t-Orbital speed (m/s): " << endl;
    cin >> os;
    cout << "\t-Period of revolution (years): " << endl;
    cin >> pr;
    cout << "\t-Escape Velocity (km/s): " << endl;
    cin >> ev;
    cout << "\t-Semimajor Axis (km): " << endl;
    cin >> sa;
    cout << "\t-Orbit Inclination (degree): " << endl;
    cin >> oi;
    cout << "\t-Perihelion (km): " << endl;
    cin >> p;
    cout << "\t-Aphelion (km): " << endl;
    cin >> a;
    cout << "\t-Orbiting: " << endl;
    cin >> o;

    NaturalObjects* newNO = new NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz, o);
    editc.addNewNaturalBody(newNO);
}
/*
 *      ###### MAN MADE OBJECT ######
 */

/**
 * Metodo que vai mostrar a lista de man made object
 */
void EditSolarSystemUI::showManMadeObject() {
    list<Objects*> aux;
    maxMMO = editc.alterManMadeObject();
    aux = editc.getMMO();
    list<Objects*>::iterator i;
    int cont = 0;
    for (i = aux.begin(); i != aux.end(); ++i) {
        cont++;
        cout << "\t" << cont << "- " << *i << endl;
        cout << "\n" << endl;
    }
}

void EditSolarSystemUI::addNewManMadeObject() {
    double v, m, g, o;
    int id;

    cout << "Velocity: " << endl;
    cin >> v;
    cout << "Mass: " << endl;
    cin >> m;
    cout << "Gravitational force: " << endl;
    cin >> g;
    cout << "Orbit: " << endl;
    cin >> o;
    cout << "ID: " << endl;
    cin >> id;

    Objects* Newobj = new Objects(v, m, g, o, id);
    editc.addNewManMadeObject(Newobj);
}

SolarSystem* EditSolarSystemUI::getSS() {
    return SS;
}

void EditSolarSystemUI::setSS(SolarSystem* solar) {
    SS = solar;
}

void EditSolarSystemUI::setMaxSS(int num) {
    maxSS = num;
}

int EditSolarSystemUI::getMaxSS() {
    return maxSS;
}
