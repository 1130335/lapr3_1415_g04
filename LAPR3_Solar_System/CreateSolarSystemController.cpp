#include "CreateSolarSystemController.h"

CreateSolarSystemController::CreateSolarSystemController() {

}

CreateSolarSystemController::~CreateSolarSystemController() {

}

void CreateSolarSystemController::createStar(string n, double m, double ed) {
    Star s(n, m, ed);
    ss.setStar(s);
}

void CreateSolarSystemController::createPlanet(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Planet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createMinorPlanet(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new MinorPlanet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createAsteroid(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o, double t) {
    ss.addToNaturalObj(new Asteroid(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o, t));
}

void CreateSolarSystemController::createFireball(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Fireball(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createMeteoroid(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Meteoroid(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createBody(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Body(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createBolide(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Bolide(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createMeteor(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Meteor(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createMeteorite(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addToNaturalObj(new Meteorite(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

void CreateSolarSystemController::createComet(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, double eo, string o) {
    ss.addToNaturalObj(new Comet(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, eo, o));
}

void CreateSolarSystemController::createMoon(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) {
    ss.addMoontoBody(new Moon(n, px, py, pz, vx, vy, vz, pr, os, iao, sa, e, oi, ed, m, d, ev, p, a, o));
}

bool CreateSolarSystemController::nObjNotEmpty() {
    return ss.nObjNotEmpty();
}

void CreateSolarSystemController::createSatellite(double m, double o, double v, double f, int id) {
    ss.addToObjects(new Satellite(v, m, f, o, id));
}

void CreateSolarSystemController::createSpaceShip(double m, double o, double v, double f, int id) {
    ss.addToObjects(new SpaceShip(v, m, f, o, id));
}

void CreateSolarSystemController::addSSToList(SolarSystemList& ssl) {
    stringstream s;
    list<SolarSystem*> l = ssl.getSSL();
    s << "Solar_System_" << l.size() + 1;
    string name = s.str();
    ss.setName(name);
    SolarSystem* ssp = &ss;
    ssl.add(ssp);
    
}

void CreateSolarSystemController::createCSVFile() {
    ss_file.createCSV_SS(ss);
}
