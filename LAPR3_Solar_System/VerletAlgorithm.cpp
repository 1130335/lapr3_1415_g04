#include "VerletAlgorithm.h"

VerletAlgorithm::VerletAlgorithm() {

}

VerletAlgorithm::~VerletAlgorithm() {

}

double VerletAlgorithm::getDelta_T() {
    return delta_T;
}

double VerletAlgorithm::getR_t() {
    return r_t;
}

double VerletAlgorithm::getT_f() {
    return t_f;
}

double VerletAlgorithm::getT_s() {
    return t_s;
}

double VerletAlgorithm::getTimestep() {
    return timestep;
}

vector<Force*> VerletAlgorithm::getForce() {
    return force;
}

vector<double> VerletAlgorithm::getForce_x() {
    return force_x;
}

vector<double> VerletAlgorithm::getForce_x2() {
    return force_x2;
}

vector<double> VerletAlgorithm::getForce_y() {
    return force_y;
}

vector<double> VerletAlgorithm::getForce_y2() {
    return force_y2;
}

vector<double> VerletAlgorithm::getForce_z() {
    return force_z;
}

vector<double> VerletAlgorithm::getForce_z2() {
    return force_z2;
}

void VerletAlgorithm::setSimulation(Simulation* s) {
    this->s = s;
}

double VerletAlgorithm::CalTimeStep() {
    t_f = s->getEnd_Time();
    t_s = s->getStart_Time();
    r_t = s->getReference_time();
    delta_T = s->getDelta_Time();
    timestep = (((t_f * 3600) -(r_t * 3600)) / (delta_T * 3600));

    return timestep;
}

vector<double> VerletAlgorithm::ComputeForce(vector<double> mass, vector<double> x, vector<double> y, vector<double> z) {
    vector<double> force;
    double xi = 0;
    double yi = 0;
    double zi = 0;
    double dist;
    double grav;

    for (int j = 0; j < mass.size(); j++) {
        //        Force *f = new Force();
        //        force.push_back(f);
        for (int i = 0; i < mass.size(); i++) {

            if (i != j) {
                dist = sqrt(pow(x[i] - x[j], 2) + pow(y[i] - y[j], 2) + pow(z[i] - z[j], 2));
                grav = (-G * mass[i] * mass[j] / pow(dist, 3));
                if ((x[i] - x[j]) != 0) {
                    //                    xi = force[j]->getFX();
                    xi = xi + grav;
                    force[j] = (xi + grav * (x[j] - x[i]));
                }
            } else {
                force[j] = xi;
            }
            if ((y[i] - y[j]) != 0) {
                //                yi = force[j]->getFY();
                yi = yi + grav;
                force[j] = (yi + grav * (y[j] - y[i]));

            } else {
                force[j] = yi;
            }

            if ((z[i] - z[j]) != 0) {
                //                zi = force[j]->getFZ();
                zi = zi + grav;
                force[j] = (zi + grav * (z[j] - z[i]));

            } else {
                force[j] = zi;
            }
        }
        force_x.push_back(xi);
        force_y.push_back(yi);
        force_z.push_back(zi);
        force_x2.push_back(xi);
        force_y2.push_back(yi);
        force_z2.push_back(zi);
    }
    return force;
}

void VerletAlgorithm::Integrate() {
    int timestep = CalTimeStep();
    t_s = s->getStart_Time();
    t_f = s->getEnd_Time();
    delta_T = s->getDelta_Time();
    r_t = s->getReference_time();
    vector<double> f1;
    vector<double> f2;
    vector<double> vxIni;
    vector<double> vyIni;
    vector<double> vzIni;
    vector<double> vxFin;
    vector<double> vyFin;
    vector<double> vzFin;
    vector<double> xIni;
    vector<double> yIni;
    vector<double> zIni;
    vector<double> xFin;
    vector<double> yFin;
    vector<double> zFin;
    list<Objects*> v_o = ss->getObj();

    for (NaturalObjects *obj : ss->getNObj()) {
        Results r;

        xIni.push_back(obj->getPx());
        yIni.push_back(obj->getPy());
        zIni.push_back(obj->getPz());
        vxIni.push_back(obj->getVx());
        vyIni.push_back(obj->getVy());
        vzIni.push_back(obj->getVz());
        mass.push_back(obj->getMass());
    }
    f1 = ComputeForce(mass, xIni, yIni, zIni);

    for (r_t = t_s; timestep <= t_f; timestep = timestep + delta_T) {
        for (int i = 0; i < v_o.size(); i++) {

            xFin[i] = xIni[i] + vxIni[i] * delta_T + (f1[i] / mass[i]) *(pow(delta_T, 2) / 2);
            yFin[i] = yIni[i] + vyIni[i] * delta_T + (f1[i] / mass[i]) *(pow(delta_T, 2) / 2);
            zFin[i] = zIni[i] + vzIni[i] * delta_T + (f1[i] / mass[i]) *(pow(delta_T, 2) / 2);

            r->inserirXpos(xFin[i]);
            r->inserirYpos(yFin[i]);
            r->inserirZpos(zFin[i]);
        }
        r->inserirTimeStep(timestep);
    }
    f1 = f2;
    f1.clear();
    f1 = ComputeForce(mass, xFin, yFin, zFin);

    for (int i = 0; i < v_o.size(); i++) {
        vxFin[i] = vxIni[i] + (delta_T / 2) *((f2[i] / mass[i])+(f1[i] / mass[i]));
        vyFin[i] = vyIni[i] + (delta_T / 2)* ((f2[i] / mass[i])+(f1[i] / mass[i]));
        vzFin[i] = vzIni[i] + (delta_T / 2) *((f2[i] / mass[i])+(f1[i] / mass[i]));

        r->inserirXvel(vxFin[i]);
        r->inserirYvel(vyFin[i]);
        r->inserirZvel(vzFin[i]);
    }

    xIni = xFin;
    vxIni = vxFin;

    yIni = yFin;
    vyIni = vyFin;

    zIni = zFin;
    vzIni = vzFin;
}
