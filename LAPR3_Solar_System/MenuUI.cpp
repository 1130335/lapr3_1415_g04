#include "MenuUI.h"
#include "ExportUI.h"

MenuUI::MenuUI() {

}

MenuUI::~MenuUI() {

}

void MenuUI::run() {

    int r_op = 1;
    int option;
    while (r_op == 1) {
        cout << "This is a Solar System application developed for LAPR3. Please choose one of the following options: " << endl;
        cout << "0 - Exit" << endl;
        cout << "1 - Open Solar System" << endl;
        cout << "2 - Create Solar System from scratch" << endl;
        cout << "3 - Import SolarSystem from CSV file" << endl;
        cout << "4 - Copy Solar System" << endl;
        cout << "5 - Edit Solar System" << endl;
        cout << "6 - Start Simulation" << endl;
        cout << "7 - Run Simulation" << endl;
        cout << "8 - Delete Simulation" << endl;
        cout << "9 - Copy Simulation" << endl;
        cout << "10 - Export Simulation" << endl;
        cout << "11 - Edit Simulation" << endl;

        cout << "\nAnswer: ";
        cin >> option;

        switch (option) {
            case 0:
                r_op = option;
                break;
            case 1:
                openui.menu(ssl);
                break;
            case 2:
                cssui.run(ssl);
                break;
            case 3:
                issui.run(ssl);
                break;
            case 4:
                copyssui.run(ssl);
                break;
            case 5:
                editui.menu(ssl);
                break;
            case 6:
                ssui.run(ssl, sl);
                break;
            case 7:
                runui.run(sl);
            case 8:
                delui.run(sl);
                break;
            case 9:
                csui.run(sl);
                break;
            case 10:
                exui.run(sl);
                break;
            case 11:
                esui.run(sl);
                break;
        }
    }
}

