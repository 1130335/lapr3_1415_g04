#include "SolarSystemList.h"

SolarSystemList::SolarSystemList() {
    
}

SolarSystemList::SolarSystemList(list<SolarSystem*> l) {
    this->ssl = l;
}

SolarSystemList::~SolarSystemList() {
    
}

void SolarSystemList::add(SolarSystem* ss) {
    this->ssl.push_back(ss);
}

void SolarSystemList::remove() {
    this->ssl.pop_back();
}

void SolarSystemList::showSolarSystems(ostream& out) {
    list<SolarSystem*> aux = this->ssl;
    int i = 1;
    while(!aux.empty()) {
        cout << "\t" << i << " - " << aux.front()->getName() << endl;
        i++;
        aux.pop_front();
    }
}

ostream & operator<<(ostream &out, SolarSystemList& l) {
    l.showSolarSystems(out);
    return out;
}

list<SolarSystem*> SolarSystemList::getSSL() const{
    return ssl;
}

void SolarSystemList::setSSL(list<SolarSystem*> listSS){
    ssl = listSS;
}

