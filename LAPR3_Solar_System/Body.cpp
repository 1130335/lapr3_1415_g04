#include "Body.h"

Body::Body() : NaturalObjects() {

}

Body::Body(string n, double px, double py, double pz, double vx, double vy, double vz, double pr, double os, double iao, double sa, double e, double oi, double ed, double m, double d, double ev, double p, double a, string o) : NaturalObjects(n, pr, os, iao, ed, m, d, ev, sa, e, oi, p, a, px, py, pz, vx, vy, vz, o) {

}

Body::~Body() {

}

Body::Body(Body& b) : NaturalObjects(b) {

}

void Body::write(ostream& out) {
    out << "Body Type: Body" << endl;
    NaturalObjects::write(out);
    out << endl;
}

void Body::addMoons(Moon* m) {
    this->moons.push_back(m);
}

void Body::showMoons() {
    for (int i = 0; i < this->moons.size(); i++) {
        cout << *this->moons[i];
    }
}
