/* 
 * File:   EditSimulation.h
 * Author: Jony
 *
 * Created on 16 de Janeiro de 2015, 22:14
 */

#ifndef EDITSIMULATIONCONTROLLER_H
#define	EDITSIMULATIONCONTROLLER_H

#include "Simulation.h"
#include "SimulationList.h"

using namespace std;

class EditSimulationController {

private:    
    Simulation* chosen;
    
public:
    EditSimulationController();
    ~EditSimulationController();
    
    void chooseSimulation(int, SimulationList&);
    void setAttributes(string, double, double, double);
    
};

#endif	/* EDITSIMULATIONCONTROLLER_H */

