#include "SpaceShip.h"

SpaceShip::SpaceShip():Objects(){
    
}

SpaceShip::SpaceShip(double v, double m, double g, double o, int id):Objects(v, m, g, o, id){
    
}

SpaceShip::SpaceShip(const SpaceShip& sp):Objects(sp){
    
}

SpaceShip::~SpaceShip() {
    
}

void SpaceShip::write(ostream& out) const{
    out << "Object Type: SpaceShip" << endl;
    Objects::write(out);
    out << endl;
}

