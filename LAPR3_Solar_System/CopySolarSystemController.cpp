#include "CopySolarSystemController.h"


CopySolarSystemController::CopySolarSystemController() {
    
}

CopySolarSystemController::~CopySolarSystemController() {
    
}

void CopySolarSystemController::showList(SolarSystemList& ssl) {
    cout << ssl;
}

SolarSystem* CopySolarSystemController::getSolarSystem(int s, SolarSystemList& ssl) {
    int cont = 1;
    SolarSystem* ss;
    list<SolarSystem*> aux = ssl.getSSL();
    
    while(cont != s) {
        aux.pop_front();
        cont++;
    }
    
    ss = aux.front();
    
    return ss;
}

void CopySolarSystemController::copySolarSystem(SolarSystem* ss, SolarSystemList& ssl) {
    SolarSystem* sc = new SolarSystem();
    sc->setNObj(ss->getNObj());
    sc->setObj(ss->getObj());
    sc->setStar(ss->getStar());
    stringstream s;
    list<SolarSystem*> l = ssl.getSSL();
    s << "Solar_System_" << l.size() + 1;
    string name = s.str();
    sc->setName(name);
    ssl.add(sc);
}
