#include "SolarSystem.h"

SolarSystem::~SolarSystem() {

}

SolarSystem::SolarSystem(Star& s) {
    this->system_star = s;
}

SolarSystem::SolarSystem() {

}

SolarSystem::SolarSystem(const SolarSystem& ss) {
    this->nObj = ss.nObj;
    this->obj = ss.obj;
    this->system_star = ss.system_star;
}

void SolarSystem::setName(string n) {
    name = n;
}

string SolarSystem::getName() {
    return name;
}

list<NaturalObjects*> SolarSystem::getNObj() {
    return nObj;
}

void SolarSystem::setNObj(list<NaturalObjects*> newnObj) {
    this->nObj = newnObj;
}

list<Objects*> SolarSystem::getObj() {
    return obj;
}

void SolarSystem::setObj(list<Objects*> newObj) {
    this->obj = newObj;
}

void SolarSystem::addToNaturalObj(NaturalObjects* nOb) {
    this->nObj.push_back(nOb);
}

void SolarSystem::addMoontoBody(Moon* m) {
    if (typeid (*(this->nObj.front())) == typeid (Planet)) {
        (dynamic_cast<Planet*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Asteroid)) {
        (dynamic_cast<Asteroid*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Body)) {
        (dynamic_cast<Body*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Bolide)) {
        (dynamic_cast<Bolide*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Comet)) {
        (dynamic_cast<Comet*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Fireball)) {
        (dynamic_cast<Fireball*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Meteor)) {
        (dynamic_cast<Meteor*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Meteorite)) {
        (dynamic_cast<Meteorite*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Meteoroid)) {
        (dynamic_cast<Meteoroid*> (this->nObj.front()))->addMoons(m);
    } else if (typeid (*(this->nObj.front())) == typeid (Moon)) {
        (dynamic_cast<Moon*> (this->nObj.front()))->addMoons(m);
    } else {
        (dynamic_cast<MinorPlanet*> (this->nObj.front()))->addMoons(m);
    }
    addToNaturalObj(m);
}

void SolarSystem::setStar(Star& s) {
    this->system_star = s;
}

Star& SolarSystem::getStar() {
    return this->system_star;
}

void SolarSystem::addToObjects(Objects* ob) {
    this->obj.push_back(ob);

}

void SolarSystem::removeNaturalObj() {
    nObj.pop_back();
}

void SolarSystem::removeObjects() {
    obj.pop_back();
}

void SolarSystem::showInfo(ostream& out) {
    list<NaturalObjects*> aux1 = nObj;
    list<Objects*> aux2 = obj;
    cout << "Solar System Information" << endl;
    cout << "Name: " << this->getName() << endl << endl;
    cout << this->getStar() << endl;
    while (!aux1.empty()) {
        out << *aux1.front() << endl;
        if (typeid (*(this->nObj.front())) == typeid (Planet)) {
            (dynamic_cast<Planet*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Asteroid)) {
            (dynamic_cast<Asteroid*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Body)) {
            (dynamic_cast<Body*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Bolide)) {
            (dynamic_cast<Bolide*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Comet)) {
            (dynamic_cast<Comet*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Fireball)) {
            (dynamic_cast<Fireball*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Meteor)) {
            (dynamic_cast<Meteor*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Meteorite)) {
            (dynamic_cast<Meteorite*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Meteoroid)) {
            (dynamic_cast<Meteoroid*> (this->nObj.front()))->showMoons();
        } else if (typeid (*(this->nObj.front())) == typeid (Moon)) {
            (dynamic_cast<Moon*> (this->nObj.front()))->showMoons();
        } else {
            (dynamic_cast<MinorPlanet*> (this->nObj.front()))->showMoons();
        }
        aux1.pop_front();
    }
    while (!aux2.empty()) {
        out << *aux2.front() << endl;
        aux2.pop_front();
    }

}

bool SolarSystem::nObjNotEmpty() {
    if (this->nObj.empty()) {
        return false;
    }
    return true;
}

ostream & operator<<(ostream &out, SolarSystem& ss) {
    ss.showInfo(out);
    return out;
}
