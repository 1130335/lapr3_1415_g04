#include "DeleteSimulationController.h"

DeleteSimulationController::DeleteSimulationController() {
}

DeleteSimulationController::~DeleteSimulationController() {
}

list<Simulation*> DeleteSimulationController::getSimulationList(SimulationList& sl) {
    return sl.getSimulationList();
}

void DeleteSimulationController::showList(SimulationList& sl) {
    cout << sl;
}

Simulation* DeleteSimulationController::chooseSimulation(int op, SimulationList& sl) {

    int cont = 1;
    Simulation* s;
    list<Simulation*> aux = sl.getSimulationList();

    while (cont != op) {
        aux.pop_front();
        cont++;
    }

    s = aux.front();

    return s;
}

void DeleteSimulationController::deleteSimulation(int op, SimulationList& sl) {
    Simulation* s = chooseSimulation(op,sl);
    list<Simulation*>::iterator it = sl.getSimulationList().begin();
    for(it; it != sl.getSimulationList().end(); it++) {
        if((*it)->getName() == s->getName()) {
            sl.getSimulationList().erase(it);
        }
    }
    //sl.getSimulationList().erase(sl.getSimulationList().begin() + (op - 1));
}
