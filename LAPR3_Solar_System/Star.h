#ifndef STAR_H
#define	STAR_H

#include <string>
#include <iostream>

using namespace std;

class Star {
private:
    string name;
    double mass; // Kg
    double equatorial_diameter; // Km
    double position[3];

public:
    Star();
    Star(string, double, double);
    Star(Star&);
    ~Star();

    string getName();
    double getMass();
    double getEquatorialDiameter();
    void setName(string);
    void setMass(double);
    void setEquatorialDiameter(double);

    void write(ostream&);
};

ostream& operator<<(ostream &out, Star& s);

#endif